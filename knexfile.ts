require("dotenv").config();
// para migraciones y seed

module.exports = {
  client: process.env.DBCLIENT,
  connection: {
    host: process.env.DBHOST,
    database: process.env.DBDATABASE,
    user: process.env.DBUSER,
    password: process.env.DBPASSWORD,
    charset: process.env.DBCHARSET,
    options: {
      encrypt: true,
    },
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    tableName: "knex_migrations",
  }
};
