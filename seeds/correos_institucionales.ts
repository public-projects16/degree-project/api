import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";
import { ICollege } from "../src/interfaces/IBDTables";
import { colleges } from "../src/bd/catalogue";

export async function seed(knex: Knex): Promise<void> {
    return await knex(tables_names.college).del().then(() => {
        const inserted_data: Array<ICollege> = new Array<ICollege>();
        for (const current_email of colleges) inserted_data.push({email: current_email[0], institution: current_email[1], campus: current_email[2], 
        logo: current_email[3] });
        return knex(tables_names.college).insert(inserted_data);
    });
}
