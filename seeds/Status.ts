import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";
import { IStatus } from "../src/interfaces/IBDTables";
import { status } from "../src/bd/catalogue";

export async function seed(knex: Knex): Promise<void> {
    return await knex(tables_names.status).del().then(() => {
        const inserted_data: Array<IStatus> = new Array<IStatus>();
        for (const current_status of status) inserted_data.push({  description: current_status[1] });
        return knex(tables_names.status).insert(inserted_data);
    });
}
