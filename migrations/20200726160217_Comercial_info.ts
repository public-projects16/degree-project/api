import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.commercial_info, function (table) {
            table.increments("id").primary();
            table.string("name");
            table.string("web_url");
            table.string("contact_email");
            table.string("contact_phone");
            table.string("social_network");
            table.string("logo");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

