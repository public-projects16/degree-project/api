import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.product, function (table) {
            table.increments("id").primary();
            table.string("name");
            table.integer("cost");
            table.string("image");
            table.string("description");
            table.integer("amount");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

