import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.user, function (table) {
            table.increments("id").primary();
            table.string("name");
            table.string("last_name");
            table.string("email");
            table.date("birthday");
            table.string("type");
            table.integer("status");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

