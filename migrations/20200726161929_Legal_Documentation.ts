import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.legal_documentation, function (table) {
            table.increments("id").primary();
            table.string("constitutive_act");
            table.string("CFID");
            table.string("owner_id");
            table.string("proof_of_tax_address");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

