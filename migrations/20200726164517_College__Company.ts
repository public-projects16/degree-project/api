import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.college_company, function (table) {
            table.increments("id").primary();
            table.integer("company_id");
            table.integer("college_id");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

