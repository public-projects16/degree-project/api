import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.bank_info, function (table) {
            table.increments("id").primary();
            table.string("secret_key");
            table.string("openpay_id");
            table.string("clabe");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

