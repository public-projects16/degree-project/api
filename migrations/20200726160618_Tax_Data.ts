import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.tax_data, function (table) {
            table.increments("id").primary();
            table.string("phone");
            table.string("tax_rfc");
            table.string("tax_address");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

