import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.data_company, function (table) {
            table.increments("id").primary();
            table.integer("company_id");
            table.integer("commercial_info_id");
            table.integer("tax_data_id");
            table.integer("legal_documentation_id");
            table.integer("bank_information_id");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

