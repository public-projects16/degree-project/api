import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.table(tables_names.user, (table) => {
            table.foreign("status").references("id").inTable(tables_names.status).onUpdate("CASCADE").onDelete("CASCADE");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

