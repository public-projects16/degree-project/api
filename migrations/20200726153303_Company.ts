import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.company, function (table) {
            table.increments("id").primary();
            table.string("commercial_business");
            table.string("type_of_taxpayer");
            table.integer("status");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

