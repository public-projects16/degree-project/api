import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.table(tables_names.product_company, (table) => {
            table.foreign("product_id").references("id").inTable(tables_names.product).onUpdate("CASCADE").onDelete("CASCADE");
            table.foreign("company_id").references("id").inTable(tables_names.company).onUpdate("CASCADE").onDelete("CASCADE");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

