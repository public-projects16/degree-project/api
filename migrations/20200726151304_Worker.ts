import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.worker, function (table) {
            table.increments("id").primary();
            table.string("b_access_key");
            table.integer("user_id");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

