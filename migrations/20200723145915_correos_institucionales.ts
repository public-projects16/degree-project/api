import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.createTable(tables_names.college, function (table) {
            table.increments("id").primary();
            table.string("email");
            table.string("institution");
            table.string("campus");
            table.string("logo");
        })
    ]);

}


export async function down(knex: Knex): Promise<void> {
}

