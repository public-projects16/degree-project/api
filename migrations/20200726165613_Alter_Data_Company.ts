import * as Knex from "knex";
import { tables_names } from "../src/bd/tablesNames";


export async function up(knex: Knex): Promise<any> {
    return Promise.all([
        knex.schema.table(tables_names.data_company, (table) => {
            table.foreign("commercial_info_id").references("id").inTable(tables_names.commercial_info).onUpdate("CASCADE").onDelete("CASCADE");
            table.foreign("tax_data_id").references("id").inTable(tables_names.tax_data).onUpdate("CASCADE").onDelete("CASCADE");
            table.foreign("legal_documentation_id").references("id").inTable(tables_names.legal_documentation).onUpdate("CASCADE").onDelete("CASCADE");
            table.foreign("bank_information_id").references("id").inTable(tables_names.bank_info).onUpdate("CASCADE").onDelete("CASCADE");
            table.foreign("company_id").references("id").inTable(tables_names.company).onUpdate("CASCADE").onDelete("CASCADE");
        })
    ]);
}


export async function down(knex: Knex): Promise<void> {
}

