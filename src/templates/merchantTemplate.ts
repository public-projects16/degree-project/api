export class MerchantTemplate {

    public getTemplate(companyName: string, key: string | undefined) {
        return  `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <style>
            .bar {
                width: 100%;
                height: 5vh;
                background: linear-gradient(70deg, #05668d, #00a896);;
            }

            .header {
                display: flex;
                width: 10vh;
                width: 100%;
            }

            .header img {
                width: 25%;
                height: 25%;
            }

            .header h1 {
                margin-top: 5vh;
                margin-left: 15vw;
            }

            .message, .key {
                margin-top: 5vh;
                text-align: justify;
                margin-left: 3vh;
                font-size: large;
            }

            .key {
                margin-top: 3vh;
            }
        </style>
        </head>
        <body>
            
            <div class="bar"></div>
        
            <div class="header">
                <img src="https://firebasestorage.googleapis.com/v0/b/escomonedas.appspot.com/o/helix%2FhelixLogoSide.png?alt=media&token=b5061694-af30-4a2f-af78-d13cb59153c9" alt="">
                <h1>Código de acceso</h1>
            </div>
        
            <div class="message">
                ¡Hola!
            </div>

            <div class="key">
                El motivo de este correo es para notificarle que su empresa ${companyName} ha sido registrada correctamente y podrá ser consultada por
                los clientes.
            </div>
        
            <div class="key">
                Para finalizar este proceso se le asignará una clave que los trabajadores que se unan al sistema deben ingresar para poder acceder
                a su catálogo de productos.
            </div>
            
            <div class="key">
                Su clave es: <strong>${key}</strong> 
            </div>
        
        </body>
        </html>`;
    }
}

const merchantTemplate: MerchantTemplate = new MerchantTemplate();
export default merchantTemplate;