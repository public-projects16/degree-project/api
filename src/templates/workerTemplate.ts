export class WorkerTemplate {

    public getTemplate(companyName: string) {
        return `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Document</title>
            <style>
                .bar {
                    width: 100%;
                    height: 5vh;
                    background: linear-gradient(70deg, #05668d, #00a896);;
                }
        
                .header {
                    display: flex;
                    width: 10vh;
                    width: 100%;
                }
        
                .header img {
                    width: 25%;
                    height: 25%;
                }
        
                .header h1 {
                    margin-top: 5vh;
                    margin-left: 15vw;
                }
        
                .message {
                    margin-top: 5vh;
                    text-align: justify;
                    margin-left: 3vh;
                    font-size: large;
                    font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
                }
            </style>
        </head>
        <body>
            
            <div class="bar"></div>
        
            <div class="header">
                <img src="https://firebasestorage.googleapis.com/v0/b/escomonedas.appspot.com/o/helix%2FhelixLogoSide.png?alt=media&token=b5061694-af30-4a2f-af78-d13cb59153c9" alt="">
                <h1>Acceso concedido</h1>
            </div>
        
            <div class="message">
                ¡Hola! <br><br>
                El motivo de este correo es para notificarle que su petición hacia la empresa ${companyName} ha sido aprovada y ya puede acceder a su catálogo de
                productos.
            </div>
        
        </body>
        </html>`;
    }
}

const workerTemplate: WorkerTemplate = new WorkerTemplate();
export default workerTemplate;