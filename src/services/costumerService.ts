import { DBConnection } from "../bd/connection";
import { ICostumer } from "../interfaces/IBDTables";
import { tables_names } from "../bd/tablesNames";
import { messages } from "../utils/messages";
import validations from "../utils/validations";
import * as crypto from 'crypto-js'; 

import * as _ from "lodash";
import { Costumer } from "../clases/Costumer";
import { User } from "../clases/User";
import { Transaction } from "../clases/Transaction";

class CostumerService extends DBConnection {

    private success = -1;
    private errors: Array<string>;
    constructor() {
        super();
        this.errors = new Array<string>();
        this.setErrors();
    }

    /**
     * @param {ICostumer} costumer
     * @returns {Promise<any[]>}
     * @memberof CostumerService
     */
    public async insertNewCostumer(costumer: ICostumer): Promise<any[]> {
        try {
            const costumer_validated = await validations.validateCostumer(costumer);
            if (costumer_validated === this.success) {
                await this.Connection.transaction((trx) => {
                    return trx.returning("id").insert(({
                        name: costumer.user.name,
                        last_name: costumer.user.last_name,
                        email: costumer.user.email,
                        birthday: costumer.user.birthday,
                        type: costumer.user.type,
                        status: costumer.user.status
                    })).into(tables_names.user).then(async (user_id: number[]) => {
                        return trx.returning("id").insert(({
                            NIP: crypto.SHA256(costumer.NIP),
                            user_id: user_id[0]
                        })).into(tables_names.costumer)
                    });
                });
                return [true, ''];
            } else return [false, this.errors[costumer_validated]];
        }catch (err){
            throw new Error(err);
        }
    }
    
    /**
     * @param {string} email
     * @returns {Promise<number>}
     * @memberof CostumerService
     */
    public getIdByEmail(email: string): Promise<number> {
        let idPromise: Promise<number>;
        idPromise = new Promise<number>((resolve, reject) => {
            this.Connection.select("C.id").from(tables_names.costumer + " as C")
            .innerJoin(tables_names.user + " as U", "U.id", "C.id").where("U.email", email)
            .then((id: number[]) => {
                resolve(_.head(id));
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        })
        return idPromise
    }

    /**
     * @param {number} id
     * @returns {Promise<Array<any>>}
     * @memberof CostumerService
     */
    public async getTransactions(id: number): Promise<Transaction[]> {
        let transactionPromise: Promise<Transaction[]>
        transactionPromise = new Promise((resolve, reject) => {
            let user: User = new User();
            let foundTransactions: Transaction[] = [];
            user.Id = id;
            costumerService.getCostumerByUserId(user)
            .then((costumer: Costumer) => {
                return this.Connection.select("T.company_id", "T.amount", "T.costumer_id", "T.concept", "T.date", "CI.name")
                .from(tables_names.transaction + " as T")
                .innerJoin(tables_names.company + " as C", "C.id", "T.company_id")
                .innerJoin(tables_names.data_company + " as DC", "DC.company_id", "C.id")
                .innerJoin(tables_names.commercial_info + " as CI", "CI.id", "DC.commercial_info_id")
                .where("T.costumer_id", costumer.costumerId)
                .orderBy("T.date", "desc")
            })
            .then((result: any[]) => {
                _.forEach(result, (row) => {
                    let newTransaction = new Transaction();
                    newTransaction.company.Id = row["company_id"];
                    newTransaction.company.CommercialInfo.Name = row["name"];
                    newTransaction.shoppingTotal = row["amount"];
                    newTransaction.concept = row["concept"];
                    newTransaction.costumer.costumerId = row["costumer_id"];
                    newTransaction.date = row["date"];
                    foundTransactions.push(newTransaction);
                });
                resolve(foundTransactions);
            })
            .catch((error: Error) => {
                reject(error);
            })
        });
        return transactionPromise;
    }

    /**
     * @description Returns the found costumer related with the givven id
     * @param {User} user
     * @returns {Promise<Costumer>}
     * @memberof CostumerService
     */
    public getCostumerByUserId(user: User): Promise<Costumer> {
        let promise: Promise<Costumer>;
        promise = new Promise((resolve, reject) => {
            this.Connection.select("*").from(tables_names.costumer)
            .where("user_id", user.Id)
            .then((costumers: ICostumer[]) => {
                let costumer: Costumer = new Costumer();
                costumer.costumerId = !_.isUndefined(_.head(costumers)) ? _.head<any>(costumers).id : costumer.costumerId;
                resolve(costumer)
            })
            .catch((error: Error) => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return promise;
    }

    /**
     * @private
     * @memberof CostumerService
     */
    private setErrors(): void {
        this.errors.push(messages.general.birthday_error);
        this.errors.push(messages.general.null_text_error);
        this.errors.push(messages.costumer.email_error);
    }
}

const costumerService: CostumerService = new CostumerService();
export default costumerService;