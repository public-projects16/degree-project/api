import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";
import { ICollege } from "../interfaces/IBDTables";
import { messages } from "../utils/messages";

import * as _ from "lodash";

class UtilsService extends DBConnection{
    constructor() {
        super();
    }

    /**
     * @returns {Promise<Array<string>>}
     * @memberof UtilsService
     */
    public async getCollegeMails(): Promise<string[]> {
        let mailsPromise: Promise<string[]>;
        mailsPromise = new Promise((resolve, reject) => {
            this.Connection.select("email").from(tables_names.college)
            .then((colleges: ICollege[]) => {
                let resolvedMails: string[] = [];
                _.forEach(colleges, (college: ICollege) => {
                    resolvedMails.push(college.email)
                });
                resolvedMails = resolvedMails.filter((value: string, index: any, array: string[]) => array.indexOf(value) === index)
                resolve(resolvedMails);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return mailsPromise;
    }

}

const utilsService: UtilsService = new UtilsService();
export default utilsService;