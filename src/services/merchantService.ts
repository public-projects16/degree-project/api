import { DBConnection } from "../bd/connection";
import { IMerchant } from "../interfaces/IBDTables";
import { tables_names } from "../bd/tablesNames";
import { messages } from "../utils/messages";
import validations from "../utils/validations";

import * as _ from "lodash";

class MerchantService extends DBConnection {

    private success = -1;
    private errors: Array<string>;
    constructor() {
        super();
        this.errors = new Array<string>();
        this.setErrors();
    }

    /**
     * @param {IMerchant} merchant
     * @returns {Promise<Array<any>>}
     * @memberof MerchantService
     */
    public async insertNewMerchant(merchant: IMerchant): Promise<Array<any>> {
        try {
            const merchant_validated = validations.validateMerchant(merchant);
            if (merchant_validated === this.success) {
                await this.Connection.transaction((trx) => {
                    return trx.returning("id").insert(({
                        name: merchant.user.name,
                        last_name: merchant.user.last_name,
                        email: merchant.user.email,
                        birthday: merchant.user.birthday,
                        type: merchant.user.type,
                        status: merchant.user.status
                    })).into(tables_names.user).then(async (user_id: number[]) => {
                        return trx.returning("id").insert(({
                            CURP: merchant.CURP,
                            a_access_key: merchant.a_access_key,
                            user_id: user_id[0]
                        })).into(tables_names.merchant)
                    });
                });
                return [true, ''];
            } else return [false, this.errors[merchant_validated]];
        }catch (err){
            throw new Error(err);
        }
    }
    
    /**
     * @param {string} a_access_key
     * @returns {Promise<string | undefined>}
     * @memberof MerchantService
     */
    public getAccessKey(a_access_key: string): Promise<string | undefined> {
        let accesKeyPromise: Promise<string>;
        accesKeyPromise = new Promise((resolve, reject) => {
            this.Connection.select("a_access_key").from(tables_names.merchant).where("a_access_key", a_access_key)
            .then((key: IMerchant[]) => {
                if (!_.isEmpty(key)) {
                    resolve(_.head<any>(key).a_access_key)
                } else {
                    resolve('');
                }
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return accesKeyPromise;
    }

    /**
     * @param {string} a_access_key
     * @returns {Promise<IMerchant>}
     * @memberof MerchantService
     */
    public getMerchantByAccessKey(a_access_key: string): Promise<IMerchant> {
        let merchanPromise: Promise<IMerchant>;
        merchanPromise = new Promise((resolve, reject) => {
            this.Connection.select("*").from(tables_names.merchant).where("a_access_key", a_access_key)
            .then((merchants: IMerchant[]) => {
                resolve(_.head<any>(merchants));
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return merchanPromise;
    }

    /**
     * @param {number} id
     * @returns {Promise<IMerchant>}
     * @memberof MerchantService
     */
    public getMerchantById(id: number): Promise<IMerchant> {
        let merchanPromise: Promise<IMerchant>;
        merchanPromise = new Promise((resolve, reject) => {
            this.Connection.select("*").from(tables_names.merchant).where("id", id)
            .then((merchants: IMerchant[]) => {
                resolve(_.head<any>(merchants));
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return merchanPromise;
    }

    /**
     * @param {number} user_id
     * @returns {Promise<IMerchant>}
     * @memberof MerchantService
     */
    public async getMerchantInfo(user_id: number): Promise<IMerchant> {
        let merchantPromise: Promise<IMerchant>;
        merchantPromise = new Promise((resolve, reject) => {
            this.Connection.select("*").from(tables_names.merchant).where("user_id", user_id)
            .then((merchantInfo: IMerchant[]) => {
                resolve(_.head<any>(merchantInfo));
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return merchantPromise;
    }

    /**
     * @private
     * @memberof MerchantService
     */
    private setErrors(): void {
        this.errors.push(messages.general.birthday_error);
        this.errors.push(messages.general.null_text_error);
    }
}

const merchantService: MerchantService = new MerchantService();
export default merchantService;