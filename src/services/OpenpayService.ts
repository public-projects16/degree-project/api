import * as dotenv from "dotenv";
import { Company } from "../clases/company";
import { OpenPayClass } from "../clases/OpenPayClass";
import { IOpenPayCustomer, IOpenpayError } from "../interfaces/IOpenPay";
import { messages } from "../utils/messages";

dotenv.config();

const OpenpayModule = require("openpay");

export class OpenPayService {
    private openpay: any;

    constructor() {}

    public createCostumer(customer: IOpenPayCustomer): Promise<IOpenPayCustomer> {
        let createCustomerPromise: Promise<IOpenPayCustomer>;
        createCustomerPromise = new Promise((resolve, reject) => {
            this.openpay.customers.create(customer, (error: IOpenpayError, body: IOpenPayCustomer) => {
                if (error) reject(error); 
                else resolve(body);
            });
        });
        return createCustomerPromise;
    }

    public createTransaction(openpayObject: OpenPayClass, company: Company): Promise<any> {
        let createTransactionPromise: Promise<any>;
        createTransactionPromise = new Promise((resolve, reject) => {
            this.setKeys(company);
            this.openpay.charges.create(openpayObject.transaction.toJSON(openpayObject.customer), (error: IOpenpayError, body: any) =>{
                if (error) reject(messages.openpay.transaction_error);
                else resolve(body);
            });
        });
        return createTransactionPromise;
    }

    private setKeys(company: Company): void {
        this.openpay = new OpenpayModule(company.BankInformation.OpenPayId, company.BankInformation.SecreyKey);
        this.openpay.setProductionReady(false);
    }
}

const openpayService: OpenPayService = new OpenPayService();
export default openpayService;