import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";
import { ICollege, IUser } from "../interfaces/IBDTables";
import { IUserData } from "../interfaces/IRoles";
import { messages } from "../utils/messages";
import { roleNames } from "../utils/rolesNames";

import * as _ from "lodash";

class UserService extends DBConnection{
    constructor() {
        super();
    }

    /**
     * @param {string} mail
     * @returns {Promise<IUserData>}
     * @memberof UserService
     */
    public async getUserInfo(mail: string): Promise<IUserData> {
        let userPromise: Promise<IUserData>;
        userPromise = new Promise((resolve, reject) => {
            let foundUserData: IUserData;
            this.Connection.select("*").from(tables_names.user).where("email", mail)
            .then((foundUserDataArray: IUserData[]) => {
                foundUserData = { user:  _.head<any>(foundUserDataArray) };
                if (_.isEqual(foundUserData.user.type, roleNames.costumer)) {
                    return this.Connection.select("*").from(tables_names.college).where("email", mail.split("@")[1])
                }
            })
            .then((colleges: ICollege[] | undefined) => {
                if (!_.isUndefined(colleges)) {
                    foundUserData.college = _.head(colleges);
                }
            })
            .then(() => {
                resolve(foundUserData);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return userPromise;
    }

    /**
     * @param {string} mail
     * @returns {Promise<Array<any>>}
     * @memberof UserService
     */
    public async isUserRegister(mail: string): Promise<Array<any>> {
        let confirmationPromise: Promise<any[]>;
        confirmationPromise = new Promise((resolve, reject) => {
            this.Connection.select("*").from(tables_names.user).where("email", mail)
            .then((foundUser: IUser[] | undefined) => {
                if (!_.isUndefined(foundUser) && !_.isEmpty(foundUser)) {
                    resolve([true, _.head<any>(foundUser).type]);
                } else {
                    resolve([false, '']);
                }
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return confirmationPromise;
    }

    /**
     * @param {number} userId
     * @param {number} status
     * @returns {Promise<void>}
     * @memberof UserService
     */
    public changeUserStatus(userId: number, status: number): Promise<void> {
        let statusPromise: Promise<void>;
        statusPromise = new Promise((resolve, reject) => {
            this.Connection.update({ status: status}).from(tables_names.user).where("id", userId)
            .then(() => {
                resolve();
            })
            .catch(() => {
                reject(messages.worker.error_declining);
            });
        });
        return statusPromise;
    }

}

const userService: UserService = new UserService();
export default userService;