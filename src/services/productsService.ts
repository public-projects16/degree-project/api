import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";
import { Product } from "../clases/Product";
import { IMerchant, IProduct } from "../interfaces/IBDTables";
import { IExcelProducts } from "../interfaces/IExcel";
import { IFiles } from "../interfaces/IRequests";
import { IUserData } from "../interfaces/IRoles";
import { JWT } from "../utils/jwt";
import { messages } from "../utils/messages";
import validations from "../utils/validations";
import companyService from "./companyServices";
import excelService from "./excelService";
import merchantService from "./merchantService";
import userService from "./userService";

import * as _ from "lodash";
import Knex from "knex";
import { CONSTANTS } from "../utils/Const";

class ProductsService extends DBConnection{
    constructor() {
        super();
    }

    /**
     * @param {string} header
     * @returns {Promise<IProduct[]>}
     * @memberof ProductsService
     */
    public showProductsOnCompany(header: string): Promise<IProduct[]> {
        let productsPromise: Promise<IProduct[]>;
        productsPromise = new Promise<IProduct[]>((resolve, reject) => {
            userService.getUserInfo(JWT.getMailOwner(header))
            .then((userInfo: IUserData) => {
                if (userInfo.user.id)
                    return merchantService.getMerchantInfo(userInfo.user.id);
                else
                    reject(messages.general.retreiving_data_error);
            })
            .then((merchantInfo: IMerchant | undefined) => {
                if (!_.isUndefined(merchantInfo)) {
                    if (merchantInfo.id)
                        return companyService.getCompanyIdByMerchantId(merchantInfo.id)
                    else
                        reject(messages.general.retreiving_data_error);
                }
            })
            .then((companyId: number) => {
                return productsService.getProducts(companyId);  
            })
            .then((products: IProduct[]) => {
                if (products)
                    return resolve(products);
                else
                    reject(messages.general.retreiving_data_error);
            })
            .catch((error: Error) => {
                reject(error);
            });
        });
        return productsPromise;
    }

    /**
     * @param {Product} product
     * @param {number} companyId
     * @returns {Promise<string>}
     * @memberof ProductsService
     */
    public insertProduct(product: Product, companyId: number): Promise<string> {
        let insertProductPromise: Promise<string>;
        insertProductPromise = new Promise((resolve, reject) => {
            return this.Connection.transaction((trx: Knex.Transaction) => {
                return trx.returning("id").insert({
                    name: product.name,
                    description: product.description,
                    cost: product.cost,
                    amount: product.amount,
                    image: product.image
                }).into(tables_names.product)
                .then((productId: number[]) => {
                    return trx.returning('id').insert({
                        product_id: _.head(productId),
                        company_id: companyId
                    }).into(tables_names.product_company)
                    .then(() => {
                        return resolve(messages.products.product_inserted_successfully);
                    }) .catch(() => { 
                        return reject(messages.products.error_inserting); 
                    })
                })
                .catch(() => {
                    return reject(messages.products.error_inserting);
                })
            });
        });
        return insertProductPromise;
    }

    /**
     * @param {IFiles} file
     * @param {number} companyId
     * @returns {Promise<number>}
     * @memberof ProductsService
     */
    public insertMasiveProducts(file: IFiles, companyId: number): Promise<number> {
        let masiveInsertPromise: Promise<number>;
        masiveInsertPromise = new Promise<number>((resolve, reject) => {
            const products: Product[] = new Array<Product>();
            let numberOfProducts: number = CONSTANTS.ZERO;
            return excelService.getExcelInformation<IExcelProducts>(file.path)
            .then((productsArray: IExcelProducts[]) => {
                const sample: IExcelProducts[] = (productsArray) ? productsArray : new Array<IExcelProducts>();
                if (validations.validateProductsExcelHeaders(_.head<any>(sample))) {
                    numberOfProducts = productsArray.length;
                    _.forEach(productsArray, (product: IExcelProducts) => {
                        if (product.Nombre.length > CONSTANTS.ZERO) {
                            const newProduct: Product = new Product();
                            newProduct.amount = parseInt(product.Cantidad);
                            newProduct.cost = parseInt(product.Costo);
                            newProduct.description = product.Descripcion;
                            newProduct.image = product.ImagenUrl;
                            newProduct.name = product.Nombre;
                            products.push(newProduct);
                        }
                    });
                    return this.saveProducts(products, companyId);
                }
                else return reject(messages.products.excel_headers_error)
            })
            .then(() => { resolve(numberOfProducts) })
            .catch(() => { reject(messages.products.error_inserting_masive); })
        });
        return masiveInsertPromise;
    }
    
    /**
     * @param {Product} productToRemove
     * @returns {Promise<string>}
     * @memberof ProductsService
     */
    public removeProduct(productToRemove: Product): Promise<string> {
        let removePromise: Promise<string>;
        removePromise = new Promise((resolve, reject) => {
            return this.Connection(tables_names.product).where('id', productToRemove.id).del()
            .then(() => {
                resolve(messages.products.removed_successfully);
            })
            .catch(() => {
                reject(messages.products.error_removing);
            })
        });
        return removePromise;
    }

    /**
     * @param {Product} productToEdit
     * @returns {Promise<string>}
     * @memberof ProductsService
     */
    public editProduct(productToEdit: Product): Promise<string> {
        let editProductPromise: Promise<string>;
        editProductPromise = new Promise((resolve, reject) => {
            return this.Connection(tables_names.product)
            .where("id", productToEdit.id)
            .update({
                name: productToEdit.name,
                description: productToEdit.description,
                cost: productToEdit.cost,
                amount: productToEdit.amount,
                image: productToEdit.image
            })
            .then(() => {
                resolve(messages.products.edited_successfully);
            })
            .catch(() => {
                reject(messages.products.error_editing);
            })
        });
        return editProductPromise;
    }

    /**
     * @param {number} companyId
     * @returns {Promise<Product[]>}
     * @memberof ProductsService
     */
    public getProductsByCompanyId(companyId: number): Promise<Product[]> {
        let promise: Promise<Product[]>;
        promise = new Promise((resolve, reject) => {
            this.Connection.select("P.name", "P.image", "P.cost", "P.amount", "P.id", "P.description")
            .from(tables_names.product + " as P")
            .innerJoin(tables_names.product_company + " as PC", "PC.product_id", "P.id")
            .where("PC.company_id", companyId)
            .andWhere((builder) => { builder.where("P.amount", ">", CONSTANTS.ZERO) })
            .then((productsResponse: any[]) =>{
                let products: Array<Product> = new Array<Product>();
                if (_.isEmpty(productsResponse)) {
                    resolve(products);
                }
                _.map(productsResponse, (row: any) => {
                    let product: Product = new Product();
                    product.name = row["name"];
                    product.image = row["image"];
                    product.cost = row["cost"];
                    product.amount = row["amount"];
                    product.id = row["id"];
                    product.description = row["description"]
                    products.push(product);
                });
                resolve(products);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return promise;
    }

    /**
     * @description Update the product amount after a transaction is completed
     * @param {Product} product
     * @returns {Promise<Product>}
     * @memberof ProductsService
     */
    public updateQuantity(product: Product): Promise<Product> {
        let updateQuantityPromise: Promise<Product>;
        updateQuantityPromise = new Promise<Product>((resolve, reject) => {
            this.getProductById(product)
            .then((foundProduct: Product) => {
                let newQuantity = foundProduct.amount - product.amount;
                foundProduct.amount = newQuantity;
                return this.editProduct(foundProduct);
            })
            .then((response: string) => {
                resolve(product);
            })
            .catch((error: Error) => {
                reject(error);
            })
        });
        return updateQuantityPromise;
    }

    /**
     * @description Find the product related with the current product id
     * @param {Product} product
     * @returns {Promise<Product>}
     * @memberof ProductsService
     */
    public getProductById(product: Product): Promise<Product> {
        let productPromise: Promise<Product>;
        productPromise = new Promise((resolve, reject) => {
            this.Connection.select("*").from(tables_names.product)
            .where("id", product.id)
            .then((foundProducts: Product[]) => {
                const returnedProduct: Product = new Product();
                const foundProduct: any = _.head(foundProducts);
                returnedProduct.amount = foundProduct.amount;
                returnedProduct.cost = foundProduct.cost;
                returnedProduct.description = foundProduct.description;
                returnedProduct.image = foundProduct.image;
                returnedProduct.name = foundProduct.name;
                returnedProduct.id = foundProduct.id;
                resolve(returnedProduct);
            })
            .catch((error: Error) => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return productPromise;
    }

    /**
     * @private
     * @param {number} companyId
     * @returns {Promise<IProduct[]>}
     * @memberof ProductsService
     */
    private getProducts(companyId: number): Promise<IProduct[]> {
        return this.Connection.select("P.name" , "P.description", "P.cost", "P.amount", "P.id", "P.image").from(tables_names.product + " as P")
        .innerJoin(tables_names.product_company + " as PC", "PC.product_id", "P.id")
        .where("PC.company_id", companyId).orderBy('P.id', 'desc');
    }

    /**
     * @private
     * @param {Product[]} products
     * @param {number} companyId
     * @returns {Promise<any>}
     * @memberof ProductsService
     */
    private saveProducts(products: Product[], companyId: number): Promise<any> {
        const productsToSave: IProduct[] = new Array<IProduct>();
        _.forEach(products, (product: Product) => {
            productsToSave.push({
                name: product.name,
                description: product.description,
                image: product.image,
                amount: product.amount,
                cost: product.cost
            });
        });
        return this.Connection.transaction((trx: Knex.Transaction) => {
            return trx.returning('id').insert(productsToSave)
            .into(tables_names.product)
            .then((ids: number[]) => {
                const relations: Array<any> = new Array<any>();
                _.forEach(ids, (id: number) => { relations.push({ company_id: companyId, product_id: id}) });
                return trx.insert(relations).into(tables_names.product_company);
            })
        });
    }

}

const productsService: ProductsService = new ProductsService();
export default productsService;