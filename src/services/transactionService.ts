import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";

import * as _ from "lodash";
import { messages } from "../utils/messages";
import { Transaction } from "../clases/Transaction";
import productsService from "./productsService";
import { OpenPayClass } from "../clases/OpenPayClass";
import openpayService from "./OpenpayService";
import companyService from "./companyServices";
import { Company } from "../clases/company";

class TransactionService extends DBConnection{
    constructor() {
        super();
    }

    /**
     * @returns {Promise<Array<College>>}
     * @memberof CollegeService
     */
    public createTransaction(transaction: Transaction, openpay: OpenPayClass): Promise<Transaction> {
        let transactionPromise: Promise<Transaction>;
        transactionPromise = new Promise((resolve, reject) => {
            console.log(transaction.company);
            companyService.getCompanyById(transaction.company.Id)
            .then((company: Company[]) => {
                console.log(_.head(company)?.BankInformation);
                
                return openpayService.createTransaction(openpay, _.head<any>(company))
            })
            .then((givenTransaction: Transaction) => {
                resolve(givenTransaction);
            })
           .catch((error: Error) => {
               reject(error);
           })
        });
        return transactionPromise;
    }

    /**
     *
     * @description Find the transactions related with the given company id
     * @param {number} companyId
     * @returns {Promise<Transaction[]>}
     * @memberof TransactionService
     */
    public findCompanyTransactions(companyId: number): Promise<Transaction[]> {
        let promise: Promise<Transaction[]>;
        promise = new Promise((resolve, reject) => {
            const foundTransactions: Transaction[] = [];
            this.Connection.select("amount", "concept", "date").from(tables_names.transaction)
            .where("company_id", companyId)
            .then((transactions: any[]) => {
                _.forEach(transactions, (row) => {
                    const currentTransaction: Transaction = new Transaction();
                    currentTransaction.concept = row["concept"];
                    currentTransaction.date = row["date"];
                    currentTransaction.shoppingTotal = row["amount"];
                    foundTransactions.push(currentTransaction);
                });
                resolve(foundTransactions);
            })
            .catch((error) => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return promise;
    }

    private makeTransaction(transaction: Transaction): Promise<Transaction> {
        let promise: Promise<Transaction>;
        promise = new Promise((resolve, reject) => {
            this.Connection.transaction((trx) => {
                return trx.returning("id").insert({
                    company_id: transaction.company.Id,
                    costumer_id: transaction.costumer.Id,
                    amount: transaction.shoppingTotal,
                    concept: transaction.concept,
                    date: new Date()
                })
                .into(tables_names.transaction)
                .then(async (transactionId: number[]) => {
                    try {
                        transaction.id =  _.head(transactionId) ? _.head<any>(transactionId) : transaction.id;
                        for (let product of transaction.products) {
                            await productsService.updateQuantity(product);
                        }
                    } catch(error) {
                        reject(error);
                    }
                })
                .then(() => {
                    resolve(transaction);
                })
                .catch((error: Error) => {
                    reject(messages.general.persisting_data_error);
                });
            })
        });
        return promise;
    }
}

const transactionService: TransactionService = new TransactionService();
export default transactionService;