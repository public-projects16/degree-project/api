import { IWorker } from "../interfaces/IBDTables";
import validations from "../utils/validations";
import { messages } from "../utils/messages";
import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";
import Knex from "knex";

import * as _ from "lodash";
import { Company } from "../clases/company";
import { Worker } from "../clases/Worker";

class WorkerService extends DBConnection {

    private success = -1;
    private errors: Array<string>;
    
    constructor() {
        super();
        this.errors = new Array<string>();
        this.setErrors();
    }

    /**
     * @param {IWorker} worker
     * @returns {Promise<IWorker>}
     * @memberof WorkerService
     */
    public async insertNewWork(worker: IWorker): Promise<IWorker> {
        let insertPromise: Promise<IWorker>;
        insertPromise = new Promise<IWorker>((resolve, reject) => {
            validations.validateWorker(worker)
            .then((worker_validated: number) => {
                if (_.isEqual(worker_validated, this.success)) {
                    return this.Connection.transaction( async (trx: Knex.Transaction) => {
                        return trx.returning("id").insert(({
                            name: worker.user.name,
                            last_name: worker.user.last_name,
                            email: worker.user.email,
                            birthday: worker.user.birthday,
                            type: worker.user.type,
                            status: worker.user.status
                        })).into(tables_names.user)
                        .then(async (user_id: number[]) => {
                            return trx.returning("id").insert(({
                                b_access_key: worker.b_access_key,
                                user_id: _.head(user_id)
                            })).into(tables_names.worker)
                        })
                        .then(() => {});
                    });
                } else { return reject(this.errors[worker_validated]); }
            })
            .then( () => {
                resolve(worker);
            })
            .catch(() => {
                reject(messages.worker.error_registering);
            })
        });
        return insertPromise;
    }

    /**
     * @param {number} userId
     * @returns {Promise<Company>}
     * @memberof WorkerService
     */
    public getCompanyWhereWorks(userId: number): Promise<Company> {
        let workerPromise: Promise<Company>;
        workerPromise = new Promise<Company>((resolve, reject) => {
            this.Connection.select("CI.name", "CI.logo")
            .from(tables_names.company + " as C")
            .innerJoin(tables_names.worker_company + " as WC", "WC.company_id", "C.id")
            .innerJoin(tables_names.worker + " as W", "W.id", "WC.worker_id")
            .innerJoin(tables_names.user + " as U", "U.id", "W.user_id")
            .innerJoin(tables_names.data_company + " as DC", "DC.company_id", "C.id")
            .innerJoin(tables_names.commercial_info + " as CI", "CI.id", "DC.commercial_info_id")
            .where("U.id", userId)
            .then((information: any[]) => {
                let commercialInfo = _.head(information);
                if (commercialInfo) {
                    let company: Company = new Company();
                    company.CommercialInfo.Name = commercialInfo.name;
                    company.CommercialInfo.Logo = commercialInfo.logo;
                    resolve(company);
                } else {
                    reject(messages.worker.no_company_found)
                }
            })
            .catch((error) => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return workerPromise;
    }

    /**
     * @param {number} id
     * @returns {Promise<Worker>}
     * @memberof WorkerService
     */
    public getWorkerById(id: number): Promise<Worker> {
        let workerPromise: Promise<Worker>;
        workerPromise = new Promise((resolve, reject) => {
            this.Connection.select("b_access_key").from(tables_names.worker).where("id", id)
            .then((foundWorker: IWorker[]) => {
                let worker: Worker = new Worker();
                let results = _.head(foundWorker);
                if (results && results.b_access_key)
                    worker.BAccessKey = results.b_access_key;
                resolve(worker);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return workerPromise;
    }

    public getWorkerByWorkerId(workerId: number): Promise<Worker[]> {
        let workerPromise: Promise<Worker[]>;
        workerPromise = new Promise((resolve, reject) => {
            this.Connection.select("U.email", "U.id as user_id")
            .from(tables_names.user + " as U")
            .innerJoin(tables_names.worker + " as W", "W.user_id", "U.id")
            .where("W.id", workerId)
            .then((data: any[]) => {
                let workers: Worker[] = [];
                _.map(data, (row) => {
                    let worker = new Worker();
                    worker.User.Email = row["email"];
                    worker.User.Id = row["user_id"];
                    workers.push(worker);
                });
                resolve(workers);
            })
            .catch((error) => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return workerPromise;
    }

    /**
     * @private
     * @memberof WorkerService
     */
    private setErrors(): void {
        this.errors.push(messages.general.birthday_error);
        this.errors.push(messages.general.null_text_error);
        this.errors.push(messages.worker.key_error);
    }
}

const workerService: WorkerService = new WorkerService();
export default workerService;