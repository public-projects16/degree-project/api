import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";
import { College } from "../clases/College";

import * as _ from "lodash";
import { messages } from "../utils/messages";

class CollegeService extends DBConnection{
    constructor() {
        super();
    }

    /**
     * @returns {Promise<Array<College>>}
     * @memberof CollegeService
     */
    public async getNames(): Promise<Array<College>> {
        let institutesResponse: Promise<College[]>;
        const collegesResponse: Array<College> = new Array<College>();
        institutesResponse = new Promise((resolve, reject) => {
            this.Connection.select("institution").distinct().from(tables_names.college).orderBy("institution")
            .then((colleges: any[]) => {
                _.forEach(colleges, (college: any) => {
                    const newCollege = new College();
                    newCollege.Institution = college.institution;
                    collegesResponse.push(newCollege);
                });
                resolve(collegesResponse);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return institutesResponse;
    }

    /**
     * @param {string} institute
     * @returns {Promise<College[]>}
     * @memberof CollegeService
     */
    public async getCollegeByInstitution(institute: string): Promise<College[]> {
        const collegesResponse: Array<College> = new Array<College>();
        let collegePromise: Promise<College[]>;
        collegePromise = new Promise((resolve, reject) => {
            this.Connection.select("campus" , "id").from(tables_names.college)
            .where("institution", institute).orderBy("institution")
            .then((colleges: any) => {
                _.forEach(colleges, (college: any) => {
                    const newCollege = new College();
                    newCollege.Campus = college.campus;
                    newCollege.Id = college.id;
                    collegesResponse.push(newCollege);
                });
                resolve(collegesResponse);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return collegePromise;
    }

}

const collegeService: CollegeService = new CollegeService();
export default collegeService;