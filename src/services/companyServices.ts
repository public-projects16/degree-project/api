import * as _ from "lodash";

import { DBConnection } from "../bd/connection";
import { tables_names } from "../bd/tablesNames";
import { IMerchant, ICompanyData, IWorker, ICompany } from "../interfaces/IBDTables";
import validations from "../utils/validations";
import { messages } from "../utils/messages";
import { system_status } from "../utils/rolesNames";
import { Company } from "../clases/company";
import { LegalDocumentation } from "../clases/LegalDocumentation";
import { IFiles } from "../interfaces/IRequests";
import { CONSTANTS, LegalDocumentsName, UserStatus } from "../utils/Const";
import Knex from "knex";
import { College } from "../clases/College";
import { CommercialInfo } from "../clases/CommercialInfo";
import { AzureStorage } from "./AzureStorage";
import workerService from "./workerService";
import { Worker } from "../clases/Worker";
import mailService from "./MailService";
import workerTemplate from "../templates/workerTemplate";
import merchantService from "./merchantService";
import merchantTemplate from "../templates/merchantTemplate";

export interface IMerchantCompany {
    company_id: number;
    merchant_id?: number;
}

class CompanyService extends DBConnection{
    constructor() {
        super();
    }

    /**
     * @param {number} merchant_id
     * @returns {(Promise<Company | undefined>)}
     * @memberof CompanyService
     */
    public getComercialInformation(merchant_id: number): Promise<Company | undefined> {
        let commercialInfoPromise: Promise<Company | undefined>;
        commercialInfoPromise = new Promise((resolve, reject) => {
            this.Connection.select("company_id").from(tables_names.merchant_company).where("merchant_id", merchant_id)
            .then((foundMerchantCompany: IMerchantCompany[]) => {
                if (_.head(foundMerchantCompany)) {
                    return this.Connection.select("C.id as company_id", "C.status", "CI.name", "CI.web_url", "CI.contact_email", "CI.contact_phone", "CI.social_network", "CI.id", "CI.logo")
                    .from(tables_names.commercial_info + " as CI")
                    .innerJoin(tables_names.data_company + " as DC", "DC.commercial_info_id", "CI.id")
                    .innerJoin(tables_names.company +  " as C", "C.id", "DC.company_id")
                    .where("DC.company_id", _.head<any>(foundMerchantCompany).company_id)
                }
            })
            .then((gottenInfo: Array<any> | undefined) => {
                if (!_.isUndefined(gottenInfo)) {
                    const companies: Array<Company> = new Array<Company>();
                    _.map(gottenInfo, (row) => {
                        const company: Company = new Company();
                        company.CommercialInfo = new CommercialInfo();
                        company.CommercialInfo.Name = row["name"];
                        company.CommercialInfo.WebUrl = row["web_url"];
                        company.CommercialInfo.ContactMail = row["contact_email"];
                        company.CommercialInfo.ContactPhone = row["contact_phone"];
                        company.CommercialInfo.SocialNetwork = row["social_network"];
                        company.CommercialInfo.Logo = row["logo"];
                        company.CommercialInfo.Id = row["id"];
                        company.Status = row["status"];
                        company.Id = row["company_id"];
                        companies.push(company);
                    });
                    resolve(_.head(companies));
                }
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return commercialInfoPromise;
    }

    /**
     * @param {Company} company
     * @param {IMerchant} merchant
     * @returns {Promise<any>}
     * @memberof CompanyService
     */
    public registerCompany(company: Company, merchant: IMerchant, userMail: string): Promise<any> {
        let companyId: number | undefined;
        let commercialInfoId: number | undefined; 
        let success_message = new Promise<any>((resolve, reject) => {
            validations.validateCompanyData(company).then(async() => {
                await this.Connection.transaction(async (trx: Knex.Transaction) => {
                    return trx.returning("id").insert({
                        commercial_business: company.CommercialInfo.CommercialBusiness,
                        type_of_taxpayer: company.TaxData.TypeOfTaxpayer,
                        status: system_status.enabled
                    }).into(tables_names.company).then(async (company_id: number[]) => {
                        companyId = _.head(company_id);
                        return trx.returning("id").insert({
                            clabe: company.BankInformation.CLABE,
                            secret_key: company.BankInformation.SecreyKey,
                            openpay_id: company.BankInformation.OpenPayId
                        }).into(tables_names.bank_info).then(async (bank_info_id: number[]) => {
                            return trx.returning("id").insert({
                                name:  company.CommercialInfo.Name,
                                web_url: company.CommercialInfo.WebUrl,
                                contact_email:  company.CommercialInfo.ContactMail,
                                contact_phone:  company.CommercialInfo.ContactPhone,
                                social_network:  company.CommercialInfo.SocialNetwork,
                            }).into(tables_names.commercial_info).then( async (commercial_info_id: number[]) => {
                                commercialInfoId = _.head(commercial_info_id);
                                return trx.returning("id").insert({
                                    phone: company.TaxData.Phone,
                                    tax_rfc: company.TaxData.TaxRfc,
                                    tax_address: company.TaxData.TaxAddress,
                                }).into(tables_names.tax_data).then(async (tax_data_id: number[]) => {
                                    return trx.returning("id").insert({
                                        company_id: _.head(company_id),
                                        commercial_info_id: _.head(commercial_info_id),
                                        tax_data_id: _.head(tax_data_id),
                                        bank_information_id: _.head(bank_info_id),
                                    }).into(tables_names.data_company).then(() => {
                                        return trx.returning("id").insert({
                                            merchant_id: merchant.id,
                                            company_id: _.head(company_id)
                                        }).into(tables_names.merchant_company);
                                    });
                                });
                            });
                        });
                    });
                })
                .then(() => {
                    return merchantService.getMerchantById(merchant.id ? merchant.id : CONSTANTS.INITIAL_ID);
                })
                .then((info: IMerchant) => {
                    return mailService.sendEmail(userMail, 'Empresa registrada', merchantTemplate.getTemplate(company.CommercialInfo.Name, info.a_access_key));
                })
                .then(() => {
                    resolve({companyId: companyId, commercialInfoId: commercialInfoId});
                }).catch((error: Error) => { reject(messages.company.error_registering); });
            }).catch((err: Error) =>{  reject(err); });
        });
        return success_message;
    }

    /**
     * @param {string} email
     * @returns {Promise<ICompanyData>}
     * @memberof CompanyService
     */
    public getCompanyDataByUserId(email: string): Promise<ICompanyData> {
        let companyDataPromise: Promise<ICompanyData>;
        companyDataPromise = new Promise( async (resolve, reject) => {
            this.Connection
            .select("DC.company_id", "DC.commercial_info_id", "DC.tax_data_id", "DC.legal_documentation_id", "DC.bank_information_id")
            .from(tables_names.user + " as U")
            .innerJoin(tables_names.merchant + " as M", "M.user_id", "U.id")
            .innerJoin(tables_names.merchant_company + " as MC", "MC.merchant_id", "M.id")
            .innerJoin(tables_names.data_company + " as DC", "MC.company_id", "DC.comapny_id")
            .where("U.email", email)
            .then((companyData: ICompanyData[]) => {
                if (!_.isUndefined( _.head(companyData)) )
                    resolve(_.head<any>(companyData));
                else
                    reject(messages.general.retreiving_data_error);
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return companyDataPromise;
    }

    /**
     * @param {number} merchantId
     * @returns {Promise<any>}
     * @memberof CompanyService
     */
    public getCompanyIdByMerchantId(merchantId: number): Promise<any> {
        let companyIdPromise: Promise<number>;
        companyIdPromise = new Promise<number>((resolve, reject) => {
            this.Connection(tables_names.merchant_company)
            .select("company_id").where("merchant_id", merchantId)
            .then((companyData: IMerchantCompany[]) => {
                if (_.isUndefined( _.head<any>(companyData).company_id ))
                    reject(messages.general.retreiving_data_error);
                else
                    resolve(_.head<any>(companyData).company_id);
            })
           .catch(() => {
                reject(messages.general.retreiving_data_error);
           })
        });
        return companyIdPromise;
    }

    /**
     * @param {*} files
     * @param {number} companyId
     * @returns {Promise<any>}
     * @memberof CompanyService
     */
    public registerDocuments(files: any, companyId: number): Promise<void> {
        let registerPromise: Promise<void>;
        registerPromise = new Promise((resolve, reject) => {
            this.getLegalDocumentsObject(files, companyId)
            .then((documents: LegalDocumentation) => {
                return this.Connection.transaction(async (trx: Knex.Transaction) => {
                    return trx.returning("id").insert({
                        constitutive_act: documents.ConstitutiveAct,
                        CFID:  documents.CFID,
                        owner_id:  documents.OwnerId,
                        proof_of_tax_address: documents.ProofOfTaxAddress
                    }).into(tables_names.legal_documentation)
                    .then((legalDocumentationId: number[]) => {
                        return trx(tables_names.data_company)
                        .update({ legal_documentation_id: _.head(legalDocumentationId)})
                        .where("company_id", companyId)
                    });
                })
            })
            .then(() => {
                resolve()
            })
            .catch((err: Error) => {
                reject(messages.company.error_registering)
            })
        });
        return registerPromise;
    }

    /**
     * @param {College} college
     * @param {number} companyId
     * @returns {Promise<string>}
     * @memberof CompanyService
     */
    public joinCompanyCollege(college: College, companyId: number): Promise<string> {
        let joinCompanyCollegePromise: Promise<string>;
        joinCompanyCollegePromise = new Promise((resolve, reject) => {
            this.Connection.insert({
                company_id: companyId,
                college_id: college.Id
            }).into(tables_names.college_company)
            .then(() => {
                resolve(messages.company.registered_successfully);
            }).catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return joinCompanyCollegePromise;
    }

    /**
     * @param {CommercialInfo} commercialInfo
     * @returns {Promise<void>}
     * @memberof CompanyService
     */
    public updateCommercialInfo(commercialInfo: CommercialInfo): Promise<void> {
        let updateCommercialInfoPromise: Promise<void>;
        updateCommercialInfoPromise = new Promise((resolve, reject) => {
            validations.validateCommercialInfo(commercialInfo).then(() => {
                return this.Connection(tables_names.commercial_info).update({
                  web_url: commercialInfo.WebUrl,
                  contact_email: commercialInfo.ContactMail,
                  contact_phone: commercialInfo.ContactPhone,
                  social_network: commercialInfo.SocialNetwork  
                })
                .where("id", commercialInfo.Id)
                .then(() => { resolve(); })
                .catch(() => { reject(messages.company.commercial_info.error_updating)})
            })
            .catch((error: Error) => {
                reject(error);
            })
        });
        return updateCommercialInfoPromise;
    }

    /**
     * @param {number} collegeId
     * @returns {Promise<any>}
     * @memberof CompanyService
     */
    public getCompanyDataByCompanyId(collegeId: number): Promise<any> {
        return this.Connection.select( "CI.social_network", "CI.contact_phone", "CI.contact_email", "CI.web_url", "CI.name", "CI.id", "CI.logo")
        .from(tables_names.company)
        .innerJoin(tables_names.college_company + " as CC", "Company.id", "CC.company_id")
        .innerJoin(tables_names.data_company + " as DC", "CC.company_id", "DC.company_id")
        .innerJoin(tables_names.commercial_info + " as CI", "DC.commercial_info_id", "CI.id")
        .where("CC.college_id", collegeId)
        .andWhere((value) => {
            value.where("Company.status", UserStatus.available)
        })
    }

    /**
     * @param {string} path
     * @param {number} commercialInfoId
     * @param {string} companyId
     * @returns {Promise<string>}
     * @memberof CompanyService
     */
    public registerLogo(path: string, commercialInfoId: number, companyId: string): Promise<string> {
        let registerLogoPromise: Promise<string>;
        registerLogoPromise = new Promise((resolve, reject) => {
            let storage: AzureStorage = new AzureStorage();
            let containerName = 'company' + companyId;
            storage.verifyGivenContainer(containerName) 
            .then((exist: boolean) => {
                if (!exist) return storage.createNewContainer(containerName); 
                else return;
            })
            .then(() => {
                return storage.saveFileOnStorage(containerName, path);
            })
            .then((newPath: string) => {
                return this.Connection(tables_names.commercial_info).update({
                    logo: newPath
                }).where("id", commercialInfoId);
            })
            .then(() => {
                resolve(messages.company.commercial_info.logo_updated_successfully)
            })
            .catch((error: Error) => {
                reject(messages.company.commercial_info.error_updating_logo);
            })
        });
        return registerLogoPromise;
    }

    /**
     * @param {number} userId
     * @returns {Promise<IWorker[]>}
     * @memberof CompanyService
     */
    public getProspects(userId: number): Promise<IWorker[]> {
        let prospectsPromise: Promise<IWorker[]>;
        prospectsPromise = new Promise((resolve, reject) => {
            this.Connection.select("M.a_access_key")
            .from(tables_names.merchant + " as M").where("user_id", userId)
            .then((keys: any[]) => {
                let a_access_key: string = _.head(keys).a_access_key;
                return this.Connection.select("U.name", "U.last_name", "U.email", "U.type", "U.status", "W.user_id", "W.id")
                            .from(tables_names.worker + " as W")
                            .innerJoin(tables_names.user + " as U", "W.user_id", "U.id")
                            .where("W.b_access_key", a_access_key)
                            .andWhere("U.status", system_status.waiting);
            })
            .then((candidates: IWorker[]) => {
                resolve(candidates);
            })
            .catch((error) => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return prospectsPromise;
    }

    /**
     * @param {number} workerId
     * @returns {Promise<void>}
     * @memberof CompanyService
     */
    public acceptWorker(workerId: number): Promise<void> {
        let acceptPromise: Promise<void>;
        acceptPromise = new Promise((resolve, reject) => {
            let company: Company;
            workerService.getWorkerById(workerId)
            .then((worker: Worker) => {
                return this.getCompanyRelatedWithAccessKey(worker.BAccessKey)
            })
            .then((foundCompany: Company) => {
                company = foundCompany;
                this.Connection.transaction((trx: Knex.Transaction) => {
                    return trx.returning("id").insert({
                        worker_id: workerId,
                        company_id: foundCompany.Id
                    })
                    .into(tables_names.worker_company)
                    .then(() => {
                        return trx.update({status: system_status.enabled}).from(tables_names.user)
                    });
                })
                .then(() => {
                    return this.getCompanyById(company.Id);
                })
                .then((companyInformation: Company[]) => {
                    company = _.head<any>(companyInformation);
                    return workerService.getWorkerByWorkerId(workerId);
                })
                .then((workers: Worker[]) => {
                    let worker = _.head(workers);
                    if (!_.isUndefined(worker)) {
                        return mailService.sendEmail(worker.User.Email, 'Registro aceptado.', workerTemplate.getTemplate(company.CommercialInfo.Name));
                    }
                })
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    reject(err);
                })
            })
            .catch((err: Error) => {
                reject(err);
            })
        });
        return acceptPromise;
    }

    /**
     * @param {string} key
     * @returns {Promise<Company>}
     * @memberof CompanyService
     */
    public getCompanyRelatedWithAccessKey(key: string): Promise<Company> {
        let promiseCompanyRelatedPromise: Promise<Company>;
        promiseCompanyRelatedPromise = new Promise((resolve, reject) => {
            this.Connection.select("C.id").from(tables_names.company + " as C")
            .innerJoin(tables_names.merchant_company + " as MC", "MC.company_id", "C.id")
            .innerJoin(tables_names.merchant + " as M", "M.id", "MC.merchant_id")
            .where("M.a_access_key", key)
            .then((foundCompany: ICompany[]) => {
                let company: Company = new Company();
                if (!_.isUndefined(_.head(foundCompany))) {
                    company.Id = _.head<any>(foundCompany).id;
                } else {
                    reject(messages.worker.key_error);
                }
                resolve(company);
            });
        });
        return promiseCompanyRelatedPromise;
    }

    /**
     * @param {number} userId
     * @returns {Promise<Company>}
     * @memberof CompanyService
     */
    public getCompanyByWorker(userId: number): Promise<Company> {
        let promise: Promise<Company>;
        promise = new Promise((resolve, reject) => {
            this.Connection.select("WC.company_id", "CI.name", "CI.logo").from(tables_names.user + " as U")
            .innerJoin(tables_names.worker + " as W", "W.user_id", "U.id")
            .innerJoin(tables_names.worker_company + " as WC", "WC.worker_id", "W.id")
            .innerJoin(tables_names.data_company + " as DC", "DC.company_id", "WC.company_id")
            .innerJoin(tables_names.commercial_info + " as CI", "CI.id", "DC.commercial_info_id")
            .where("U.id", userId)
            .then((workerCompanyResponse: any[]) => {
                let company: Company = new Company();
                if (_.isEmpty(workerCompanyResponse)) {
                    resolve(company)
                }
                company.Id = _.head(workerCompanyResponse).company_id;
                company.CommercialInfo.Name = _.head(workerCompanyResponse).name;
                company.CommercialInfo.Logo = _.head(workerCompanyResponse).logo;
                resolve(company);
            })
            .catch((error: Error) => {
                reject(messages.general.retreiving_data_error);
            })
        });
        return promise;
    }

    /**
     * @param {number} companyId
     * @param {number} status
     * @returns {Promise<void>}
     * @memberof CompanyService
     */
    public changeCompanyStatus(companyId: number, status: number): Promise<void> {
        let statusPromise: Promise<void>;
        statusPromise = new Promise((resolve, reject) => {
            this.Connection.update({ status: status}).from(tables_names.company).where("id", companyId)
            .then(() => {
                resolve();
            })
            .catch(() => {
                reject(messages.general.retreiving_data_error);
            });
        });
        return statusPromise;
    }

    /**
     * @description Get the company information according the company id
     * @param {number} companyId
     * @returns {Promise<Company[]>}
     */
    public getCompanyById(companyId: number): Promise<Company[]> {
        let promise: Promise<Company[]>;
        promise = new Promise((resolve, reject) => {
            let companies: Company[] = [];
            this.Connection.select("C.id as company_id", "C.status", "CI.name", "CI.web_url", "CI.contact_email", "CI.contact_phone", "CI.social_network", "CI.id", "CI.logo"
            ,"BI.openpay_id", "BI.secret_key")
            .from(tables_names.company + " as C")
            .innerJoin(tables_names.data_company + " as DC", "DC.company_id", "C.id")
            .innerJoin(tables_names.commercial_info + " as CI", "CI.id", "DC.commercial_info_id")
            .innerJoin(tables_names.bank_info + " as BI", "BI.id", "DC.bank_information_id")
            .where("C.id", companyId)
            .then((data: any[]) => {
                _.map(data, (row) => {
                    let company = new Company();
                    company.CommercialInfo.Name = row["name"];
                    company.CommercialInfo.SocialNetwork = row["social_network"];
                    company.CommercialInfo.ContactPhone = row["contact_phone"];
                    company.CommercialInfo.ContactMail = row["contact_email"];
                    company.CommercialInfo.WebUrl = row["web_url"];
                    company.BankInformation.OpenPayId = row["openpay_id"];
                    company.BankInformation.SecreyKey = row["secret_key"];
                    company.Status = row["status"];
                    company.Id = row["company_id"];
                    companies.push(company);
                });
            })
            .then(() => {
                resolve(companies);
            })
            .catch((error) => {
                reject(messages.general.retreiving_data_error);
            })
        })
        return promise;
    }

    /**
     * @private
     * @param {*} files
     * @param {number} companyId
     * @returns {Promise<LegalDocumentation>}
     * @memberof CompanyService
     */
    private getLegalDocumentsObject(files: any, companyId: number): Promise<LegalDocumentation> {
        let legalDocumentationPromise: Promise<LegalDocumentation>;
        legalDocumentationPromise = new Promise<LegalDocumentation>(async (resolve, reject) => {
            const legalDocument: LegalDocumentation = new LegalDocumentation();
            let storage = new AzureStorage();
            let containerName = 'company' + companyId.toString();
            storage.verifyGivenContainer(containerName)
            .then((exist) => {
                if (!exist)
                    return storage.createNewContainer(containerName);
                return;
            })
            .then(() => {
                const currentFile: IFiles = _.head<any>(files[LegalDocumentsName.cfid]);
                return storage.saveFileOnStorage(containerName, currentFile.path);
            })
            .then((cfidLink: string) => {
                legalDocument.CFID = cfidLink;
                const currentFile: IFiles = _.head<any>(files[LegalDocumentsName.constitutiveAct]);
                return storage.saveFileOnStorage(containerName, currentFile.path);
            })
            .then((constitutuveActLink: string) => {
                legalDocument.ConstitutiveAct = constitutuveActLink;
                const currentFile: IFiles = _.head<any>(files[LegalDocumentsName.ownerId]);
                return storage.saveFileOnStorage(containerName, currentFile.path);
            })
            .then((ownerIdLink: string) => {
                legalDocument.OwnerId = ownerIdLink;
                const currentFile: IFiles = _.head<any>(files[LegalDocumentsName.proofOfTaxAddress]);
                return storage.saveFileOnStorage(containerName, currentFile.path);
            })
            .then((proofLink) => {
                legalDocument.ProofOfTaxAddress = proofLink;
                resolve(legalDocument);
            })
            .catch(() => {
                reject();
            })
        });
        return legalDocumentationPromise;
    }
}

const companyService: CompanyService = new CompanyService();
export default companyService;