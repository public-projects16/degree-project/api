import { BlobServiceClient, BlockBlobUploadResponse, ContainerCreateResponse } from '@azure/storage-blob';
import fs from "fs";
import * as dotenv from "dotenv";
import * as _ from "lodash";

dotenv.config();

export class AzureStorage {

    private blobServiceClient: BlobServiceClient;
    private containerClient: any;

    constructor() {
        const conecctionString: string = (process.env.ConnectionStringStorage) ? process.env.ConnectionStringStorage : '';
        this.blobServiceClient = BlobServiceClient.fromConnectionString(conecctionString);
    }

    /**
     * @param {string} containerName
     * @returns {Promise<ContainerCreateResponse>}
     * @memberof AzureStorage
     */
    public createNewContainer(containerName: string): Promise<ContainerCreateResponse> {
        let newContainerPromise: Promise<ContainerCreateResponse>;
        newContainerPromise = new Promise<ContainerCreateResponse>((resolve, reject) => {
            this.containerClient = this.blobServiceClient.getContainerClient(containerName);
            this.containerClient.create()
            .then((createResponse: ContainerCreateResponse) => {
                this.containerClient.setAccessPolicy("blob");
                resolve(createResponse);
            })
            .catch((error: Error) => {
                reject(error);
            })
        });
        return newContainerPromise;
    }

    /**
     * @param {string} containerName
     * @returns {Promise<boolean>}
     * @memberof AzureStorage
     */
    public verifyGivenContainer(containerName: string): Promise<boolean> {
        let verifyGivenContainerPromise: Promise<boolean>;
        verifyGivenContainerPromise = new Promise( async (resolve, reject) => {
            let containerExists: boolean = false;
            let containers = this.blobServiceClient.listContainers();
            let currentContainer = await containers.next();
            while(!currentContainer.done) {
                if (_.isEqual(currentContainer.value.name, containerName)) {
                    containerExists = true;
                    this.containerClient = this.blobServiceClient.getContainerClient(containerName);
                    break;
                }
                currentContainer = await containers.next();
            }
            resolve(containerExists);
        });
        return verifyGivenContainerPromise;
    }

    /**
     * @param {string} containerName
     * @param {string} path
     * @returns {Promise<string>}
     * @memberof AzureStorage
     */
    public saveFileOnStorage(containerName: string, path: string): Promise<string> {
        let saveFileResponse: Promise<string>;
        saveFileResponse = new Promise<string>((resolve, reject) => {
            let fileData =  fs.readFileSync(path);
            let originalName = this.getFilename(path);
            let blockBlobClient = this.containerClient.getBlockBlobClient(originalName);
            blockBlobClient.upload(fileData, fileData.length)
            .then((uploadResponse: BlockBlobUploadResponse) => {
                resolve(process.env.AzureAddress + containerName + "/" + originalName);
            })
            .catch((error: Error) => {
                reject(error);
            })
        });
        return saveFileResponse;     
    }

    /**
     * @private
     * @param {string} path
     * @returns {string}
     * @memberof AzureStorage
     */
    private getFilename(path: string): string {
        let directories: Array<string> =  path.split("\\");
        return directories[directories.length - 1];
    }
}