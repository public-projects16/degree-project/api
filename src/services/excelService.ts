import * as dotenv from "dotenv";
import { messages } from "../utils/messages";

dotenv.config();

const excelConversor = require("xlsx-to-json");

class ExcelService{

    constructor() {}

    /**
     * @template T
     * @param {string} path
     * @returns {Promise<T[]>}
     * @memberof ExcelService
     */
    public getExcelInformation<T>(path: string): Promise<T[]> {
        let excelInformationPromise: Promise<T[]>
        excelInformationPromise = new Promise<T[]>((resolve, reject) => {
            excelConversor({
                input: path,
                output: "../../output.json"
            }, (error: Error, result: Array<T>) => {
                if (error)
                    reject(messages.products.excel_error);
                else
                    resolve(result);
            });
        });
        return excelInformationPromise;
    }
}

const excelService: ExcelService = new ExcelService();
export default excelService;