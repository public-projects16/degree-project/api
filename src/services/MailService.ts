import * as dotenv from "dotenv";
import * as  mailer from "nodemailer";
import smtpTransport from "nodemailer-smtp-transport";

dotenv.config();

export class MailService {

    private transporter: any;
    private mailOptions: object = {};

    constructor() {
        this.createTransporter();
    }

    /**
     * @description Send a mail with tge message and header to the destination mal
     * @param {string} destinationMail
     * @param {string} header
     * @param {*} message
     * @returns {Promise<void>}
     * @memberof MailService
     */
    public sendEmail(destinationMail: string, header: string, message: string): Promise<void> {
        let mailPromise: Promise<void>;
        mailPromise = new Promise((resolve, reject) => {
            this.setMailOptions(destinationMail, header, message);
            this.transporter.sendMail(this.mailOptions, (error: Error, info: any) => {
                if (error) reject(error);
                else resolve();
            });
        });
        return mailPromise;
    }

    /**
     * @description Create a new transporter
     * @return {void}
     */
    private createTransporter(): void {
        this.transporter = mailer.createTransport(smtpTransport({
            service: process.env.MAIL_SERVICE,
            auth: {
                user: process.env.HELIX_MAIL,
                pass: process.env.HELIX_MAIL_PASS
            }
        }));
    }

    /**
     * @description Set the header and html message to send
     * @param {string} destinationMail
     * @param {string} header
     * @param {string} message
     * @memberof MailService
     */
    private setMailOptions(destinationMail: string, header: string, message: string) {
        this.mailOptions = {
            from: "no-reply < " + process.env.HELIX_MAIL + ">",
            to: destinationMail,
            subject: header,
            html: message
        };
    }
}

const mailService: MailService = new MailService();
export default mailService;
