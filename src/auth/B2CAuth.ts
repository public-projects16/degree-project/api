import passport from "passport";
import { BearerStrategy, IBearerStrategyOptionWithRequest } from "passport-azure-ad";
import * as dotenv from "dotenv";

dotenv.config();

const options: IBearerStrategyOptionWithRequest = {
    identityMetadata: "https://" + process.env.B2C_DOMAINHOST + "/" + process.env.TENANT_ID_GUID + "/" + process.env.POLICY_NAME + "/v2.0/.well-known/openid-configuration/",
    clientID: (process.env.CLIENT_ID) ? process.env.CLIENT_ID : "",
    policyName: process.env.POLICY_NAME,
    isB2C: true,
    validateIssuer: false,
    loggingLevel: "warn",
    loggingNoPII: false,
    passReqToCallback: true,
    allowMultiAudiencesInToken: true,
    audience: process.env.CLIENT_ID
};

const authenticationStrategy = new BearerStrategy(options, (req, token, done) => {
    if (!token.oid)
        done(new Error("Expected oid in token but no token/id found."));
    else {
        done(undefined, token, {
            id: token.oid,
        });
    }
});

passport.use(authenticationStrategy);

export { authenticationStrategy };