const  multer = require("multer");
import * as dotenv from "dotenv";
import { LegalDocumentsName } from "../utils/Const";

dotenv.config();

export class MulterConfiguration {
    private companyDocumentsStore: any;
    private companyDocumentsUpload: any
    private companyLogoUpload: any;
    private productFileUpload: any;
    private productStore: any;
    private companyLogo: any;

    constructor() {
        this.setStorage();
        this.setUpload();
        this.setProductStorage();
        this.setLogoStorage();
        this.setProduct();
        this.setLogo();
    }

    /**
     * @readonly
     * @memberof MulterConfiguration
     */
    get CompanyDocumentsUpload() { return this.companyDocumentsUpload; }

    /**
     * @readonly
     * @memberof MulterConfiguration
     */
    get ProductFileUpload() { return this.productFileUpload; }

    /**
     * @readonly
     * @memberof MulterConfiguration
     */
    get CompanyLogo() { return this.companyLogo; }

    /**
     * @private
     * @memberof MulterConfiguration
     */
    private setStorage(): void {
        this.companyDocumentsStore = multer.diskStorage({
            limits: {
                fileSize: 8000000
             },
            destination:function(req1: any,file: any,cb: any){
                if (file.mimetype !== 'application/pdf')
                    return cb(new Error('El archivo debe ser pdf'), '');
                else
                    cb(null, process.cwd() + '/upload');
            },
            filename:function(req: any,file: any,cb: any){
                cb(null, Date.now() + '.' + file.originalname);
            }
        });
    }

    /**
     * @private
     * @memberof MulterConfiguration
     */
    private setProductStorage(): void {
        this.productStore = multer.diskStorage({
            limits: {
                fileSize: 8000000
             },
            destination:function(req2: any,file: any,cb: any){
                if (file.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    return cb(new Error('El archivo debe tener la extensión xlsx'), '');
                else
                    cb(null, process.cwd() + '/upload');
            },
            filename:function(req: any,file: any,cb: any){
                cb(null, Date.now() + '.' + file.originalname);
            }
        });
    }

    /**
     * @private
     * @memberof MulterConfiguration
     */
    private setLogoStorage(): void {
        this.companyLogoUpload = multer.diskStorage({
            limits: {
                fileSize: 8000000
             },
            destination:function(req3: any, file: any,cb: any){
                    cb(null, process.cwd() + '/upload');
            },
            filename:function(req: any,file: any,cb: any){
                cb(null, Date.now() + '.' + file.originalname);
            }
        });
    }

    /**
     * @private
     * @memberof MulterConfiguration
     */
    private setUpload() : void {
        this.companyDocumentsUpload = multer({ 
            storage: this.companyDocumentsStore,
            limits: {
                fileSize: 8000000
             }
            }).fields([
            { name: LegalDocumentsName.cfid , maxCount: 1 },
            { name: LegalDocumentsName.constitutiveAct , maxCount: 1 },
            { name: LegalDocumentsName.ownerId , maxCount: 1 },
            { name: LegalDocumentsName.proofOfTaxAddress , maxCount: 1 },
        ]);
    }

    /**
     * @private
     * @memberof MulterConfiguration
     */
    private setProduct(): void {
        this.productFileUpload = multer({
            storage: this.productStore,
            limits: {
                fileSize: 8000000
             }
        }).fields([
            {name: 'products', maxCount: 1}
        ]);
    }

    /**
     * @private
     * @memberof MulterConfiguration
     */
    private setLogo(): void {
        this.companyLogo = multer({
            storage: this.companyLogoUpload,
            limits: {
                fileSize: 8000000
             }
        }).fields([
            {name: 'image', maxCount: 1}
        ]);
    }
}

const multerConfig: MulterConfiguration = new MulterConfiguration();
export default multerConfig;