import express, { Application } from "express";
import cors from "cors";
import * as dotenv from "dotenv";
import * as swagger from "swagger-express-ts";
import { swagger_config } from "./docs/configuration";
import proofRoutes from "./routes/proofRoutes";
import example_routes from "./routes/exampleRoutes";
import costumerRoutes from "./routes/costumerRoutes";
import workerRoutes from "./routes/workerRoutes";
import merchantRoutes  from "./routes/merchantRoutes";
import userRoutes from "./routes/userRoutes";
import collegeRoutes from "./routes/collegeRoutes";
import productsRoutes from "./routes/productsRoutes";
import companyRoutes from "./routes/companyRoutes";
import transactionRoutes from "./routes/transactionRoutes";
import passport from "passport";
import { AuthController } from "./controller/authController";
import * as bodyParser from "body-parser";
import morgan from "morgan";
import cookieParser from "cookie-parser";

dotenv.config();

class Server {

    private app: Application;
    private b2cController: AuthController;


    constructor() {
        this.app = express();
        this.b2cController = new AuthController();
        this.configuration();
        this.setAllowedHeaders();
        this.routes();
    }

    /**
     * @memberof Server
     */
    public start(): void {
        this.app.listen( this.app.get("port"), () => {
            console.log("Running on port %d", this.app.get("port"));
            console.log("Press CTRL + C to stop it");
        });
    }

    /**
     * @private
     * @memberof Server
     */
    private configuration() {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(cors({origin: true}));
        this.app.use(morgan("dev"));
        this.app.use(cookieParser());
        this.app.use(express.json());
        this.app.use(passport.initialize());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use((process.env.API_DOCS) ? process.env.API_DOCS : '' , express.static("swagger"));
        this.app.use("/api-docs/swagger/assets", express.static("node_modules/swagger-ui-dist"));
        this.app.use(swagger.express(swagger_config));
    }

    /**
     * @private
     * @memberof Server
     */
    private setAllowedHeaders(): void {
        this.app.use((req, res, next) => {
            res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
            next();
          });
    }

    /**
     * @private
     * @memberof Server
     */
    private routes() {
        this.app.use(proofRoutes);
        this.app.use("/api", example_routes);
        this.app.use("/costumer", passport.authenticate("oauth-bearer", {session: false}), costumerRoutes);
        this.app.use("/worker",   workerRoutes);
        this.app.use("/merchant", passport.authenticate("oauth-bearer", {session: false}), merchantRoutes);
        this.app.use("/user", passport.authenticate("oauth-bearer", {session: false}), userRoutes);
        this.app.use("/company", passport.authenticate("oauth-bearer", {session: false}),  companyRoutes);
        this.app.use("/college", passport.authenticate("oauth-bearer", {session: false}),  collegeRoutes);
        this.app.use("/products", passport.authenticate("oauth-bearer", {session: false}), productsRoutes);
        this.app.use("/transactions", transactionRoutes);
    }
}

const server: Server = new Server();
export default server;