import knex from "knex";
import * as dotenv from "dotenv";
dotenv.config();

export class DBConnection {

    private connection: knex;

    constructor() {
        this.connection = knex({
            client: process.env.DBCLIENT,
            connection: {
                host: process.env.DBHOST,
                database: process.env.DBDATABASE,
                user: process.env.DBUSER,
                password: process.env.DBPASSWORD,
                charset: process.env.DBCHARSET,
                options: {
                    encrypt: true
                }
            },
            pool: {
                min: 2,
                max: 10
              },
              acquireConnectionTimeout: 10000
        });
    }

  /**
   * @readonly
   * @type {knex}
   * @memberof DBConnection
   */
  get Connection(): knex {
      return this.connection;
  }

}
