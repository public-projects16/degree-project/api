import { IResponse } from "../interfaces/IResponse";

class Responses {
    constructor() {}

    /**
     * @param {string} message
     * @param {Array<any>} [info=[]]
     * @param {number} [code=200]
     * @returns {IResponse}
     * @memberof Responses
     */
    public getSuccessResponse(message: string, info: Array<any> = [], code = 200) : IResponse {
        return {
            status: "success",
            data: {
                info: info,
                code: code,
                message: message
            }
        }
    }

    /**
     * @param {*} info
     * @param {number} [code=404]
     * @returns {IResponse}
     * @memberof Responses
     */
    public getErrorResponse(info:  any, code = 404) : IResponse {
        return {
            status: "error",
            error: {
                info: info,
                code: code
            }
        }
    }

    /**
     * @param {*} info
     * @param {number} [code=202]
     * @returns {IResponse}
     * @memberof Responses
     */
    public getWarnResponse(info:  any, code = 202) : IResponse {
        return {
            status: "warn",
            error: {
                info: info,
                code: code
            }
        }
    }
}

const responses: Responses = new Responses();
export default responses;