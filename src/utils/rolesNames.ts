export const roleNames = {
    costumer: 'costumer',
    worker: 'worker',
    merchant: 'merchant'
}

export const system_status = {
    enabled: 1,
    disabled: 2,
    waiting: 3
}