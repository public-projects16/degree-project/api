import * as dotenv from "dotenv";
import  jwt from 'jsonwebtoken';

dotenv.config();

export class JWT {

    private letters: Array<string>;
    private numbers: Array<string>;
    private symbols: Array<string>;

    constructor() {
        this.letters = new Array<string>();
        this.numbers = new Array<string>();
        this.symbols = new Array<string>();
    }

    /**
     * @static
     * @param {string} header
     * @returns {(string | any)}
     * @memberof JWT
     */
    public static decodeHeaderToken(header: string): string | any {
        const token = header.split(" ")[1];
        return  jwt.decode(token);
    }

    /**
     * @static
     * @param {string} header
     * @returns {string}
     * @memberof JWT
     */
    public static getMailOwner(header: string): string {
        const token = header.split(" ")[1];
        const decoded_token: any = jwt.decode(token);
        return decoded_token.emails[0];
    }

    /**
     * @static
     * @returns {string}
     * @memberof JWT
     */
    public static keyGenerator(mail: string, role: string): string {
        let key: string =  process.env.JWT_PASS ?  process.env.JWT_PASS : '';
        return jwt.sign({ mail : mail, role: role }, key , { algorithm: 'HS256'});
    }
}