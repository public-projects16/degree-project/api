export enum CONSTANTS {
    INITIAL_ID = -1,
    ZERO = 0,
    FIRST_POSITION = 0,
    SECOND_POSITION = 1,
    THIRD_POSITION =2
}

export enum UserStatus {
    available = 1,
    disabled = 2,
    waiting = 3,
    rejected = 4
}

export const CompanyDataNames = {
    id: 'id',
    typeOfTaxpayer: 'typeOfTaxpayer',
    commercialInfo: 'commercialInfo',
    taxData: 'taxData',
    legalDocumentation: 'legalDocumentation',
    bankInformation: 'bankInformation'
};

export const TaxDataNames = {
    id: 'id',
    phone: 'phone',
    taxAddress: 'taxAddress',
    taxRfc: 'taxRfc',
    typeOfTaxpayer: 'typeOfTaxpayer'
};

export const BankInformationNames = {
    id: 'id',
    secretKey: 'secretKey',
    openpayId: 'openpayId',
    clabe: 'clabe'
};

export const CommercialInfoNames = {
    id : 'id',
    name : 'name',
    webUrl : 'webUrl',
    contactMail : 'contactMail',
    contactPhone : 'contactPhone',
    socialNetwork : 'socialNetwork',
    commercialBusiness : 'commercialBusiness'
};

export const LegalDocumentsName = {
    id: 'id',
    constitutiveAct: 'constitutiveAct',
    cfid: 'cfid',
    ownerId: 'ownerId',
    proofOfTaxAddress: 'proofOfTaxAddress',
};

export const ExcelProductsNames = {
    fieldname: 'products',
    imageUrl: 'ImagenUrl',
    description: 'Descripcion',
    cost: 'Costo',
    amount: 'Cantidad'
}