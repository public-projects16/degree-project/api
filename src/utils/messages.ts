export const messages = {
    general: {
        auth_error: 'Error de autorización.',
        register_error: 'Error al registrarse, favor de verificar tus datos.',
        birthday_error: 'Debes tener 18 años al menos para poder registrarte',
        null_text_error: 'Debes llenar todos los campos.',
        registered: 'Usuario registrado.',
        not_registered: 'Usuario no registrado.',
        oauth_error: 'Error de autenticación.',
        retreiving_data_error: 'Ocurrio un error al obtener la información, favor de intentarlo más tarde.',
        persisting_data_error: 'Ocurrio un error al guardar la información, favor de intentarlo de nuevo.'
    },
    costumer: {
        added_successfully: 'Cliente registrado correctamente.',
        email_error: 'El correo registrado debe ser institucional. Favor de iniciar sesión con un correo válido.'
    },
    merchant: {
        added_successfully: 'Comerciante registrado con éxito.',
        not_registered: 'Comerciante no registrado'
    },
    worker: {
        key_error: 'No se encontró ninguna empresa con la clave insertada. Favor de verificarla.',
        added_successfully: 'Trabajador registrado con éxito. Se le notificará cuando la empresa acepte su solicitud.',
        error_registering: 'Ocurrió un error al registrar un nuevo trabajador, favor de intentarlo más tarde.',
        no_company_found: 'Aún no se encuentra registrado en alguna empresa. Esto se puede deber a que su solicitud aún no es aprobada o fue rechazada.',
        accepted_successfully: 'Se ha aceptado el trabajador de manera exitosa.',
        error_declining: 'Ocurrio un error al declinar la petición del trabajador',
        declined_successfully: 'Solicitud rechazada'
    },
    company: {
        documents_format_error: 'Los documentos deben ser en formato pdf, favor de verificarlos',
        disable_error: 'Ocurrio un error deshabilitando la empresa',
        dusabled_successfully: 'Empresa deshabilitada exitosamente',
        reactivated_successfully: 'Empresa reactivada.',
        error_reactivating: 'Ocurrio un error al reactivar la empresa',
        bank_data: {
            wrong_clabe: 'Se debe ingresar la clabe bancaria',
            secret_key: 'Se debe ingresar la llave proporcionada por OpenPay',
            no_bank_data: 'No se encontro información bancaria',
            openpay_id_error: 'Se debe ingresar el id de cliente proporcionado por OpenPay',
        },
        commercial_info: {
            no_commercial_info: 'No se encontró información comercial',
            wrong_name: 'Se debe introducir el nombre del comercio',
            wrong_social_network: 'El formato de la url de la página de facebook no es el correcto. Favor de verificarlo.',
            wrong_email: 'El email de contacto no cumple con el formato',
            wrong_phone: 'El teléfono de contacto no cumple con el formato',
            wrong_web_url: 'El formato de la url de su empresa no es el correcto. Favor de verificarlo.',
            error_registering: 'Ocurrió un error al registrar los documentos, favor de intentarlo más tarde.',
            error_updating: 'Ocurrió un error actualizando la información comercial.',
            updated_correctly: 'Información comercial actualizada con éxito.',
            logo_error: 'El formato del logo no es el indicado. Favor de verificarlo',
            logo_updated_successfully: 'Logo editado exitosamente',
            error_updating_logo: 'Ocurrio un error al editar el logo, favor de intentar más tarde.'
        },
        tax_data: {
            no_tax_data: 'No se encontro información físcal',
            wrong_phone: 'El teléfono de contacto físcal no cumple con el formato. Favor de verificarlo.',
            wrong_rfc: 'Se debe introducir un RFC válido',
            wrong_tax_address: 'Se debe introducir la dirección físcal'
        },
        not_registered: 'No se tiene una empresa registrada',
        registered_successfully: 'Empresa registrada con éxito',
        error_registering: 'Ocurrió un error al registrar la empresa. Favor de intentarlo más tarde.'
    },
    products: {
        excel_error: 'El contenido del archivo no es válido. Favor de verificarlo.',
        excel_headers_error: 'Las cabeceras del archivo no son las correctas, favor de verificarlas.',
        error_inserting_masive: 'Ocurrió un error al agregar los productos.',
        masive_success_registering: 'Productos registrados con éxito.',
        product_inserted_successfully: 'El producto se agrego con éxito',
        error_inserting: 'Error al insertar el producto.',
        removed_successfully: 'Producto eliminado correctamente.',
        error_removing: 'Ocurrió un error al eliminar el producto.',
        edited_successfully: 'Producto editado exitósamente.',
        error_editing: 'Ocurrió un error el editar el producto.'
    },
    account: {
        disabled_successfully: 'Cuenta deshabilitado con éxito',
        disabled_error: 'Ocurrio un error al deshabilitar la cuenta.',
        reactivated_succesfully: 'Cuenta reactivada con éxito',
        reactivation_error: 'Ocurrio un error al reactivar la cuenta'
    },
    transactions: {
        created_successfully: 'Transacción creada con éxito'
    },
    openpay: {
        transaction_error: "Ocurrio un error al procesar la transaccion, favor de intentarlo más tarde."
    }
}