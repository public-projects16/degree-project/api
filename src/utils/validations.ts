import { ICostumer, IMerchant, IWorker } from "../interfaces/IBDTables";
import merchantService from "../services/merchantService";
import utilsService from "../services/utilsService";
import { messages } from "./messages";
import { Company } from "../clases/company";
import { CommercialInfo } from "../clases/CommercialInfo";
import { TaxData } from "../clases/TaxData";
import { BankInformation } from "../clases/BankInformation";

import * as _ from 'lodash';
import RE2 from "re2";
import { IExcelProducts } from "../interfaces/IExcel";

class Validations {

    private min_age = 18;
    private success = -1;
    private birthday_error = 0;
    private nullText = 1;
    private key_error = 2;
    private email_error = 2;

    constructor() {}

    /**
     * @private
     * @param {string} birthday
     * @returns {number}
     * @memberof Validations
     */
    private validateBirthday(birthday: string): number {
        const current_date = new Date();
        const year = birthday.split("-")[0];
        const month = birthday.split("-")[1];
        const day = birthday.split("-")[2];
        let response = this.success;
        const year_rest =  current_date.getFullYear() -  parseInt(year);
        if (year_rest < this.min_age)
            return this.birthday_error;
        else if (year_rest === this.min_age) {
            const month_rest =  current_date.getMonth() - parseInt(month);
            if (month_rest < 0)
                response = this.birthday_error;
            else if (month_rest === 0) {
                const day_rest = current_date.getDate() - parseInt(day);
                response = (day_rest > 0) ? response : this.birthday_error;
            }
            return response;
        } else return response;
    }

    /**
     * @private
     * @param {string} value
     * @returns {boolean}
     * @memberof Validations
     */
    private validateStrings(value: string): boolean {
        return value.length !== 0;
    }

    /**
     * @private
     * @param {string} a_access_key
     * @returns {Promise<boolean>}
     * @memberof Validations
     */
    private async validateAAccessKey(a_access_key: string): Promise<boolean> {
        const key = await merchantService.getAccessKey(a_access_key);
        return !_.isEqual(key, undefined);
    }

    /**
     * @private
     * @param {string} email
     * @returns {Promise<boolean>}
     * @memberof Validations
     */
    private async validateCostumerEmail(email: string): Promise<boolean> {
        let mails = await utilsService.getCollegeMails();
        return mails.includes(email.split('@')[1]) && this.validateEmail(email);
    }

    /**
     * @private
     * @param {string} url
     * @returns {boolean}
     * @memberof Validations
     */
    private validateUrl(url: string): boolean {
        const regx = new RE2(/[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/);
        return regx.test(url);
    }

    /**
     * @private
     * @param {string} mail
     * @returns {boolean}
     * @memberof Validations
     */
    private validateEmail(mail: string): boolean {
        const regx = new RE2(/^\w+([\.-]*\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/);
        return regx.test(mail);
    }

    /**
     * @private
     * @param {string} phone
     * @returns {boolean}
     * @memberof Validations
     */
    private validatePhone(phone: string): boolean {
        const regx = new RE2(/^([0-9]*[-.\s()]*)+$/);
        return regx.test(phone);
    }

    /**
     * @param {ICostumer} costumer
     * @returns {Promise<number>}
     * @memberof Validations
     */
    public async validateCostumer(costumer: ICostumer): Promise<number> {
        let response = this.validateBirthday(costumer.user.birthday);
        response = (this.validateStrings(costumer.user.name)) ? response : this.nullText;
        response = (this.validateStrings(costumer.user.last_name)) ? response : this.nullText;
        response = (await this.validateCostumerEmail(costumer.user.email)) ? response : this.email_error;
        response = (this.validateStrings(costumer.NIP)) ? response : this.nullText;
        return response;
    }

    /**
     * @param {IMerchant} merchant
     * @returns {number}
     * @memberof Validations
     */
    public validateMerchant(merchant: IMerchant): number {
        let response = this.validateBirthday(merchant.user.birthday);
        response = (this.validateStrings(merchant.user.name)) ? response : this.nullText;
        response = (this.validateStrings(merchant.user.last_name)) ? response : this.nullText;
        response = (this.validateEmail(merchant.user.email)) ? response : this.nullText;
        response = (this.validateStrings(merchant.CURP)) ? response : this.nullText;
        return response;
    }

    /**
     * @param {IWorker} worker
     * @returns {Promise<number>}
     * @memberof Validations
     */
    public  validateWorker(worker: IWorker): Promise<number> {
        let workerValidationPromise: Promise<number>;
        let key: string = ( worker.a_access_key ) ? worker.a_access_key : '';
        workerValidationPromise = new Promise((resolve, reject) => {
            this.validateAAccessKey(key)
            .then((isValidKey: boolean) => {
                let response = this.validateBirthday(worker.user.birthday);
                response = (this.validateStrings(worker.user.name)) ? response : this.nullText;
                response = (this.validateStrings(worker.user.last_name)) ? response : this.nullText;
                response = (this.validateEmail(worker.user.email)) ? response : this.nullText;
                response = response && isValidKey ? response : this.key_error;
                resolve(response);
            })
            .catch((error) => {
                reject(messages.general.retreiving_data_error);
            })
        })
        return workerValidationPromise;
    }

    /**
     * @param {Company} company
     * @returns {Promise<boolean>}
     * @memberof Validations
     */
    public  validateCompanyData(company: Company): Promise<boolean> {
        let response: Promise<boolean>;
        response = new Promise<boolean>((resolve, reject) => {
            this.validateCommercialInfo(company.CommercialInfo)
            .then(() => {
                return this.validateTaxData(company.TaxData);
            })
            .catch((error: Error) => { return reject(error); })
            .then(() => {
                return this.validateBankData(company.BankInformation);
            })
            .catch((error: Error) => { return reject(error); })
            .then(() => {
                resolve(true);
            })
            .catch((error: Error) => { return reject(error); });
        });
        return response;
    }

    /**
     * @param {CommercialInfo} [commercial_info]
     * @returns {Promise<boolean>}
     * @memberof Validations
     */
    public validateCommercialInfo(commercial_info?: CommercialInfo): Promise<boolean> {
        let response: Promise<boolean>;
        response = new Promise((resolve, reject) => {
            if (commercial_info) {
                if (!this.validateStrings(commercial_info.Name)) reject(messages.company.commercial_info.wrong_name);
                if (!this.validateUrl(commercial_info.SocialNetwork)) reject(messages.company.commercial_info.wrong_social_network);
                if (!this.validateEmail(commercial_info.ContactMail)) reject(messages.company.commercial_info.wrong_email);
                if (!this.validatePhone(commercial_info.ContactPhone)) reject(messages.company.commercial_info.wrong_phone);
                if (!this.validateUrl(commercial_info.WebUrl)) reject(messages.company.commercial_info.wrong_web_url);
                resolve(true);
            } else reject(messages.company.commercial_info.no_commercial_info);
        });
        return response;
    }

    /**
     * @param {TaxData} [tax_data]
     * @returns {Promise<boolean>}
     * @memberof Validations
     */
    public validateTaxData(tax_data?: TaxData): Promise<boolean> {
        let response: Promise<boolean>;
        response = new Promise((resolve, reject) => {
            if (tax_data) {
                if ( !this.validatePhone(tax_data.Phone)) reject(messages.company.tax_data.wrong_phone);
                if ( !this.validateStrings(tax_data.TaxAddress)) reject(messages.company.tax_data.wrong_tax_address);
                if ( !this.validateStrings(tax_data.TaxRfc)) reject(messages.company.tax_data.wrong_rfc);
                resolve(true);
            } else reject(messages.company.tax_data.no_tax_data);
        });
        return response;
    }

    /**
     * @param {BankInformation} [bank_data]
     * @returns {Promise<boolean>}
     * @memberof Validations
     */
    public validateBankData(bank_data?: BankInformation): Promise<boolean> {
        let response: Promise<boolean>;
        response = new Promise((resolve, reject) => {
            if (bank_data) {
                if ( !this.validateStrings(bank_data.CLABE) ) reject(messages.company.bank_data.wrong_clabe);
                if ( !this.validateStrings(bank_data.SecreyKey)) reject(messages.company.bank_data.secret_key);
                if ( !this.validateStrings(bank_data.OpenPayId)) reject(messages.company.bank_data.openpay_id_error);
                resolve(true);
            } else reject(messages.company.bank_data.no_bank_data);
        });
        return response;
    }

    /**
     * @param {IExcelProducts} sample
     * @returns {boolean}
     * @memberof Validations
     */
    public validateProductsExcelHeaders(sample: IExcelProducts): boolean {
        return !_.isUndefined(sample.Cantidad) &&
                !_.isUndefined(sample.Costo) &&
                !_.isUndefined(sample.Descripcion) &&
                !_.isUndefined(sample.ImagenUrl)
    }
}

const validations: Validations = new Validations();
export default validations;