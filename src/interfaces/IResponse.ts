interface IDataResponse {
    info: Array<any>;
    message: string;
    code?: number;
}

interface IErrorResponse {
    info: string;
    code?: number;
}

export interface IResponse {
    status: string;
    data?: IDataResponse;
    error?: IErrorResponse;
}