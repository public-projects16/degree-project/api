export interface ICollege {
    id?: number;
    email: string;
    institution: string;
    campus: string;
    logo?: string;
}

export interface IUser {
    id?: number;
    name: string;
    last_name: string;
    email: string;
    birthday: string;
    type: string;
    status: number;
}

export interface IWorker {
    id?: number;
    b_access_key?: string;
    a_access_key?: string;
    company_id?: number;
    user: IUser;
}

export interface ICostumer {
    id?: number;
    user: IUser;
    NIP: string;
}

export interface IMerchant {
    id?: number;
    user: IUser;
    a_access_key?: string;
    CURP: string;
}

export interface ITransaction {
    id?: number;
    costumer_id?: number;
    company_id: number;
    amount: number;
    concept: string;
    date: Date;
}

export interface ICommercialInfo {
    id?: number;
    name: string;
    webUrl: string;
    contactMail: string;
    contactPhone: string;
    socialNetwork: string;
    commercialBusiness: string;
    logo?: string;
}

export interface ITaxData {
    id?: number;
    phone: string;
    taxRfc: string;
    taxAddress: string;
    typeOfTaxpayer: string;
}

export interface ILegalDocumentation {
    id?: number;
    constitutiveAct: string;
    cfid: string;
    ownerId: string;
    proofOfTaxAddress: string;
}

export interface IProduct {
    id?: number;
    name: string;
    cost: number;
    image: string;
    description: string;
    amount: number;
}

export interface IBankInformation {
    id?: number;
    secretKey: string;
    openpayId: string;
    clabe: string;
}

export interface ICompany {
    id?: number;
    commercialInfo?: ICommercialInfo;
    taxData?: ITaxData;
    legalDocumentation?: ILegalDocumentation;
    bankInformation?: IBankInformation;
}

export interface IStatus {
    id?: number;
    description: string;
}

export interface ICompanyData {
    commercialInfoId: number;
    taxDataId: number;
    legalDocumentationId: number;
    bankInformationId: number;
    merchantId: number;
}