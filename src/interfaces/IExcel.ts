export interface IExcelProducts {
    ImagenUrl: string;
    Descripcion: string;
    Costo: string;
    Cantidad: string;
    Nombre: string;
}