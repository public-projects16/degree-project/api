export interface IOpenpayError {
    http_code: number,
    error_code: number,
    category: string,
    description: string
}

export interface IOpenPayAddress {
    city: string,
    state: string,
    line1: string,
    line2: string,
    postal_code: string,
    country_code: string
}

export interface IOpenPayCustomer {
    name: string,
    email: string,
    last_name: string,
    phone_number: string,
    address: IOpenPayAddress
}
