import * as swagger from "swagger-express-ts";

export default {
  Example: {
    properties: {
          originalname: {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.ARRAY ,
            required : true,
            example: ["123456789", "12345"],
          },
          mimetype: {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
            required : true,
            enum : ["test", "dev"]
          },
          filename : {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
            required : true
          },
          path: {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
            required : true
          },
          file: {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
            required : true
          },
          size: {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
            required : true
          },
          createAt: {
            type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
            required : true
          }
      }
  },
  User: {
    properties: {
      name: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Josué"]
      },
      lastname: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Ruíz Hernández"]
      },
      email: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : false,
        example: ["josuejoshua592@gmail.com"]
      },
      birthday: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["22/03/1998"]
      },
      status: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.NUMBER ,
        required : false,
        example: [1]
      }
    }
  },
  Worker: {
    properties: {
      b_access_key: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJrZXlBY2Nlc0EifQ"]
      },
      a_access_key: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJrZXkiOiJrZXlBY2Nlc0EifQ"]
      },
      company_id: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.NUMBER ,
        required : true,
        example: [1]
      }
    }
  },
  College: {
    properties: {
      email: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["@lasallistas.org.mx"]
      },
      institution: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Universidad La Salle"]
      },
      campus: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Benjamin Franklin"]
      },
      logo: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["https://www.ulsavictoria.edu.mx/alumno/img/login_Mesa_2.png"]
      }
    }
  },
  BankInformation: {
    properties: {
      secretKey: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["dsdohdoash3123dsa"]
      },
      openpayId: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["absidopenpay1243"]
      },
      clabe: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["BBBPPPCCCCCCCCCCCD"]
      }
    }
  },
  CommercialInfo: {
    properties: {
      name: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Helix"]
      },
      webUrl: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["http://helix.com.mx"]
      },
      contactMail: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["correo@helix.com"]
      },
      contactPhone: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["55 14281550"]
      },
      socialNetwork: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["https://www.facebook.com/helix/"]
      },
      commercialBusiness: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Comerciante"]
      },
      logo: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["https://www.helix.com.mx/img/logo.png"]
      }
    }
  },
  LegalDocumentation: {
    properties: {
      constitutiveAct: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["acta constitutiva.pdf"]
      },
      cfid: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["cfid.pdf"]
      },
      ownerId: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.NUMBER ,
        required : true,
        example: [1]
      },
      proofOfTaxAddress: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["proof of tax address.pdf"]
      }
    }
  },
  TaxData: {
    properties: {
      phone: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["55 56434503"]
      },
      taxRfc: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["INC - 0012N"]
      },
      taxAddress: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Av. San Isidro Alto Lerma, Col. Olivar del Conde. C.P. 01829"]
      },
      typeOfTaxpayer: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["moral"]
      }
    }
  },
  Product: {
    properties: {
      name: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Papas sabritas"]
      },
      cost: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.NUMBER ,
        required : true,
        example: [10]
      },
      image: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["https://www.lacomer.com.mx/superc/img_art/7501011133884_3.jpg"]
      },
      description: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Papas amarillas"]
      },
      amount: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.NUMBER ,
        required : true,
        example: [15]
      }
    }
  },
  Transaction: {
    properties: {
      companyId: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.INTEGER ,
        required : true,
        example: [1]
      },
      costumerId: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.INTEGER ,
        required : true,
        example: [3]
      },
      concept: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["Venta"]
      },
      shoppingTotal: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.INTEGER ,
        required : true,
        example: [74]
      },
      openpayId: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.STRING ,
        required : true,
        example: ["AAAAFFFF"]
      },
      products: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.ARRAY ,
        required : true,
        example: [ {"name": "Leche Santa Clara",
          "id": "1",
          "amount": "2",
          "cost": "24"} ]
      }
    }
  },
  UserId: {
    properties: {
      userId: {
        type : swagger.SwaggerDefinitionConstant.Model.Property.Type.NUMBER ,
        required : true,
        example: [1]
      },
    }
  }
};
