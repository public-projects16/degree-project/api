import * as _ from 'lodash';
import { IProduct } from '../interfaces/IBDTables';
import { CONSTANTS } from '../utils/Const';

export class Product  {

    private _id: number = CONSTANTS.INITIAL_ID;
    private _name: string = "";
    private _cost: number = CONSTANTS.ZERO; 
    private _image: string =  "";
    private _description: string = "";
    private _amount: number = CONSTANTS.ZERO;

    constructor(information?: any) {
        if (!_.isUndefined(information)) this.setData(information);
    }

    get id() { return this._id; }

    set id(value: number) { this._id = value; }

    get name() { return this._name; }

    set name(value: string) { this._name = value; }

    get cost() { return this._cost; }

    set cost(value: number) { this._cost = value; }

    get image() { return this._image; }

    set image(value: string) { this._image = value; }

    get description() { return this._description; }

    set description(value: string) { this._description = value; }

    get amount() { return this._amount; }

    set amount(value: number) { this._amount = value; }

    /**
     * @private
     * @param {IProduct} information
     * @memberof Product
     */
    private setData(information: IProduct) {
        this.name = !_.isUndefined(information.name) ? information.name : this.name;
        this.description = !_.isUndefined(information.description) ? information.description : this.description;
        this.amount = !_.isUndefined(information.amount) ? information.amount : this.amount;
        this.cost = !_.isUndefined(information.cost) ? information.cost : this.cost;
        this.image = !_.isUndefined(information.image) ? information.image : this.image;
        this.id = !_.isUndefined(information.id) ? information.id : this.id;
    }
}