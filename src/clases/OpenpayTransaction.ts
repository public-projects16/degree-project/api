import { CONSTANTS } from "../utils/Const";
import { OpenPayCard } from "./OpenpayCard";
import { OpenPayCostumer } from "./OpenpayCustomer";

export class OpenPayTransaction {

    private _method: string;
    private _card: OpenPayCard;
    private _amount: number;
    private _description: string;
    private _orderId: string;
    private _module: any;
    private _deviceSessionId: string;
    private _sourceId: string;
    private _currency: string;


    constructor() {
        this._card = new OpenPayCard();
        this._method = "";
        this._amount = CONSTANTS.ZERO;
        this._description = "";
        this._orderId = "";
        this._sourceId = "";
        this._deviceSessionId = "";
        this._currency = "";
        this._orderId = "";
    }

    get deviceSessionId(): string { return this._deviceSessionId; }

    set deviceSessionId(value: string) { this._deviceSessionId = value; }

    get sourceId(): string { return this._sourceId; }

    set sourceId(value: string) { this._sourceId = value; }

    get currency(): string { return this._currency; }

    set currency(value: string) { this._currency = value; }

    get method(): string { return this._method; }

    set method(value: string) { this._method = value; }

    get card(): OpenPayCard { return this._card; }

    set card(value: OpenPayCard) { this._card = value; }

    get amount(): number { return this._amount; }

    set amount(value: number) { this._amount = value; }

    get description(): string { return this._description; }

    set description(value: string) { this._description = value; }

    get orderId(): string { return this._orderId; }

    set orderId(value: string) { this._orderId = value; }

    get module(): any { return this._module; }

    set module(value: any) { this._module = value; }

    public toJSON(customer: OpenPayCostumer): object {
        return {
            source_id: this.sourceId,
            method: this.method,
            currency: this.currency,
            device_session_id: this.deviceSessionId,
            amount : this.amount,
            description : this.description,
            order_id : this.orderId,
            customer: customer.toJSON(),
        }
    }
}