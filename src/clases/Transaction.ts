import * as _ from "lodash";
import { CONSTANTS } from "../utils/Const";

import { Company } from "./company";
import { Costumer } from "./Costumer";
import { Product } from "./Product";

export class Transaction {

    private _company: Company;
    private _costumer: Costumer;
    private _products: Array<Product>;
    private _concept: string;
    private _shoppingTotal: number;
    private _id: number;
    private _date: Date;

    constructor() {
        this._company = new Company();
        this._costumer = new Costumer();
        this._products = new Array<Product>();
        this._concept = "";
        this._shoppingTotal = CONSTANTS.ZERO;
        this._id = CONSTANTS.INITIAL_ID;
        this._date = new Date();
    }

    get id(): number { return this._id; }

    set id(id: number) { this._id = id; }
    get company(): Company { return this._company; }

    set company(company: Company) { this._company = company; }

    get costumer(): Costumer { return this._costumer; }

    set costumer(costumer: Costumer) { this._costumer = costumer; }

    get concept(): string { return this._concept; }

    set concept(concept: string) { this._concept = concept; }

    get shoppingTotal(): number { return this._shoppingTotal; }

    set shoppingTotal(total: number) { this._shoppingTotal = total; }

    get date(): Date { return this._date; }

    set date(date: Date) { this._date = date; }

    get products(): Array<Product> { return this._products; }

    set products(products: Product[]) {
        _.forEach(products, (product) => {
            let newProduct: Product = new Product();
            newProduct.name = !_.isUndefined(product.name) ? product.name : "";
            newProduct.amount = !_.isUndefined(product.amount) ? product.amount : CONSTANTS.ZERO;
            newProduct.cost = !_.isUndefined(product.cost) ? product.cost : CONSTANTS.ZERO;
            newProduct.id = !_.isUndefined(product.id) ? product.id : CONSTANTS.INITIAL_ID;
            this._products.push(newProduct);
        });
    }
}