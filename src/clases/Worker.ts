import { CONSTANTS } from "../utils/Const";
import { User } from "./User";

export class Worker {
   
    private id: number;
    private b_access_key: string;
    private a_access_key: string;
    private company_id: number;
    private user: User;

    constructor() {
        this.id = CONSTANTS.INITIAL_ID;
        this.b_access_key = "";
        this.a_access_key = "";
        this.company_id = CONSTANTS.INITIAL_ID;
        this.user = new User();
    }

    get Id(): number { return this.id; }

    set Id(value: number) { this.id = value; }

    get BAccessKey() { return this.b_access_key; }

    set BAccessKey(value: string) { this.b_access_key = value; }

    get AAccessKey() { return this.a_access_key; }

    set AAccessKey(value: string) { this.a_access_key = value; }

    get CompanyId() { return this.company_id; }

    set CompanyId(value: number) { this.company_id = value; }

    get User() { return this.user; }

    set User(value: User) { this.user = value; }
}