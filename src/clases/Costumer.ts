import { CONSTANTS } from "../utils/Const";
import { User } from "./User";

export class Costumer extends User {

    private _costumerId: number;

    constructor() {
        super();
        this._costumerId = CONSTANTS.INITIAL_ID;
    }

    get costumerId(): number { return this._costumerId; }

    set costumerId(value: number) { this._costumerId = value; }
}