import { CONSTANTS } from "../utils/Const";

export class College {

    private id: number;
    private email: string;
    private institution: string;
    private campus: string;
    private logo: string;

    constructor() {
        this.id = CONSTANTS.INITIAL_ID;
        this.email =  "";
        this.institution = "";
        this.campus = "";
        this.logo = "";
    }

    get Id() { return this.id; }
    
    set Id(id: number) { this.id = id; }

    get Email() { return this.email; }

    set Email(mail: string) { this.email = mail; }

    get Institution() { return this.institution; }

    set Institution(institution: string) { this.institution = institution; }

    get Campus() { return this.campus; }

    set Campus(campus: string) { this.campus = campus; }

    get Logo() { return this.logo; }

    set Logo(logo: string) { this.logo = logo; }
}