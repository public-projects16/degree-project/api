import * as _ from "lodash";

import { CONSTANTS } from "../utils/Const";
import { IBankInformation } from "../interfaces/IBDTables";

export class BankInformation {

    private id: number = CONSTANTS.INITIAL_ID;
    private secretKey: string = "";
    private openpayId: string = "";
    private clabe: string = "";

    constructor(data?: IBankInformation) {
        if (!_.isUndefined(data)) this.setDataValues(data);
    }

    get Id(): number { return this.id; }

    get SecreyKey(): string { return this.secretKey; }

    get CLABE(): string { return this.clabe; }

    get OpenPayId(): string { return this.openpayId; }

    set Id(id: number) { this.id = id; }

    set SecreyKey(secretKey: string) { this.secretKey = secretKey; }

    set CLABE(clabe: string) { this.clabe = clabe; }

    set OpenPayId(value: string) { this.openpayId = value; }

    /**
     * @private
     * @param {IBankInformation} data
     * @memberof BankInformation
     */
    private setDataValues(data: IBankInformation) {
        this.id = (data.id) ? data.id : CONSTANTS.INITIAL_ID;
        this.secretKey = data.secretKey;
        this.clabe = data.clabe;
        this.openpayId = data.openpayId;
    }
}