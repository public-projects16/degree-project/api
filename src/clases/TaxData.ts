import { CONSTANTS } from "../utils/Const";
import * as _ from "lodash";

import { ITaxData } from "../interfaces/IBDTables";

export class TaxData {
    
    private id: number =  CONSTANTS.INITIAL_ID;
    private phone: string =  "";
    private taxRfc: string = "";
    private taxAddress: string = "";
    private typeOfTaxpayer: string = "";

    constructor(information?: ITaxData) {
        if (!_.isUndefined(information)) this.setInformationValues(information);
    }

    get Id(): number { return this.id; }

    get Phone(): string { return this.phone; }

    get TaxRfc(): string { return this.taxRfc; }

    get TaxAddress(): string { return this.taxAddress; }

    get TypeOfTaxpayer() { return this.typeOfTaxpayer; }

    set Id(id: number) { this.id = id; }

    set Phone(phone: string) { this.phone = phone; }

    set TaxRfc(taxRfc: string) { this.taxRfc = taxRfc; }

    set TaxAddress(address: string) { this.taxAddress = address; }

    set TypeOfTaxpayer(value: string) { this.typeOfTaxpayer = value; }

    /**
     * @private
     * @param {ITaxData} data
     * @memberof TaxData
     */
    private setInformationValues(data: ITaxData): void {
        this.id = (data.id) ? data.id : CONSTANTS.INITIAL_ID;
        this.phone = data.phone;
        this.taxAddress = data.taxAddress;
        this.taxRfc = data.taxRfc;
        this.typeOfTaxpayer = data.typeOfTaxpayer;
    }
}