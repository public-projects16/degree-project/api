import * as _ from "lodash";

import { CommercialInfo } from "./CommercialInfo";
import { TaxData } from "./TaxData";
import { LegalDocumentation } from "./LegalDocumentation";
import { BankInformation } from "./BankInformation";
import { CONSTANTS } from "../utils/Const";
import { ICompany } from "../interfaces/IBDTables";

export class Company {

    private id: number =  CONSTANTS.INITIAL_ID;
    private status: number = CONSTANTS.INITIAL_ID;
    private commercialInfo: CommercialInfo = new CommercialInfo();
    private taxData: TaxData = new TaxData();
    private legalDocumentation: LegalDocumentation = new LegalDocumentation();
    private bankInformation: BankInformation = new BankInformation();

    constructor(company?: ICompany) {
        if (!_.isUndefined(company)) this.setDataValues(company);
    }

    get Id() { return this.id; }

    get CommercialInfo() { return this.commercialInfo; }

    get TaxData() { return this.taxData; }

    get LegalDocumentation() { return this.legalDocumentation; }

    get BankInformation() { return this.bankInformation; }

    set Id(value: number) { this.id = value; }

    set CommercialInfo(value: CommercialInfo) { this.commercialInfo = value; }

    set TaxData(value: TaxData) { this.taxData = value; }

    set LegalDocumentation(value: LegalDocumentation) { this.legalDocumentation = value; }

    set BankInformation(value: BankInformation) { this.bankInformation = value; }
    
    get Status() { return this.status; }

    set Status(value: number) { this.status = value; }

    /**
     * @private
     * @param {ICompany} information
     * @memberof Company
     */
    private setDataValues(information: ICompany) {
        this.id = CONSTANTS.INITIAL_ID;
        this.commercialInfo = new CommercialInfo(information.commercialInfo);
        this.taxData = new TaxData(information.taxData);
        this.legalDocumentation = new LegalDocumentation(information.legalDocumentation);
        this.bankInformation = new BankInformation(information.bankInformation);
    }
}