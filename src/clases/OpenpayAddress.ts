import { IOpenPayAddress } from "../interfaces/IOpenPay";

export class OpenPayAddress {
    
    private _city: string = "";
    private _state: string = "";
    private _line1: string = ""
    private _line2: string = "";
    private _postalCode: string = "";
    private _countryCode: string = "";

    constructor() {}

    get city() { return this._city; }

    set city(value: string) { this._city = value; }

    get state() { return this._state; }

    set state(value: string) { this._state = value; }

    get line1() { return this._line1; }

    set line1(value: string) { this._line1 = value; }

    get line2() { return this._line2; }

    set line2(value: string) { this._line2 = value; }

    get postalCode() { return this._postalCode; }

    set postalCode(value: string) { this._postalCode = value; }

    get countryCode() { return this._countryCode; }

    set countryCode(value: string) { this._countryCode = value; }

    public toJSON(): IOpenPayAddress {
        return {
            city: this.city,
            state: this.state,
            line1: this.line1,
            line2: this.line2,
            postal_code: this.postalCode,
            country_code: this.countryCode
        }
    }
}