import * as _ from "lodash";

import { CONSTANTS } from "../utils/Const";
import { ILegalDocumentation } from "../interfaces/IBDTables";

export class LegalDocumentation {

    private id: number = CONSTANTS.INITIAL_ID;
    private constitutiveAct: string = "";
    private cfid: string = "";
    private ownerId: string = "";
    private proofOfTaxAddress: string = "";

    constructor(data?: ILegalDocumentation) {
        if (!_.isUndefined(data)) this.setDataValues(data);
    }

    get Id(): number { return this.id; }

    get ConstitutiveAct(): string { return this.constitutiveAct; }

    get CFID(): string { return this.cfid; }

    get OwnerId(): string { return this.ownerId; }

    get ProofOfTaxAddress(): string { return this.proofOfTaxAddress ; }

    set Id(id: number) { this.id = id; }

    set ConstitutiveAct(constitutiveAct: string) { this.constitutiveAct = constitutiveAct; }

    set CFID(cfid: string) { this.cfid = cfid; }

    set OwnerId(ownerId: string) { this.ownerId = ownerId; }

    set ProofOfTaxAddress(proof: string) { this.proofOfTaxAddress = proof; }

    /**
     * @private
     * @param {ILegalDocumentation} data
     * @memberof LegalDocumentation
     */
    private setDataValues(data: ILegalDocumentation): void {
        this.id = (data.id) ? data.id : CONSTANTS.INITIAL_ID;
        this.cfid = data.cfid;
        this.constitutiveAct = data.constitutiveAct;
        this.ownerId = data.ownerId;
        this.proofOfTaxAddress = data.proofOfTaxAddress;
    }
}