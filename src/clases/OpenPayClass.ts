import { OpenPayCostumer } from "./OpenpayCustomer";

import * as _ from "lodash";
import { OpenPayTransaction } from "./OpenpayTransaction";

const allowedTypes = ["string", "number", "boolean"];

export class OpenPayClass {

    private _customer: OpenPayCostumer;
    private _transaction: OpenPayTransaction;

    constructor() {
       this._customer = new OpenPayCostumer();
       this._transaction = new OpenPayTransaction();
    }

    get customer () { return this._customer; }

    set customer(value: OpenPayCostumer) { this._customer = value; }

    get transaction () { return this._transaction; }

    set transaction(value: OpenPayTransaction) { this._transaction = value; }

    public toTokenize() {
        return {
            holder_name: this.customer.name,
            card_number: this.transaction.card.cardNumber,
            cvv2: this.transaction.card.cvv2,
            expiration_month: this.transaction.card.expirationMonth,
            expiration_year: this.transaction.card.expirationYear,
            address: this.customer.adress,
        }
    }

    public load(data: any) {
        _.mapKeys(this, (value, key) => {
            if (_.isEqual(typeof value, 'object')) {
                this.mapObject(value, data);
            } else {
                value = data[_.camelCase(key)];
                data[_.camelCase(key)] = value;
            }
        })
    }

    private mapObject(obj: any, data: any) {
        _.mapKeys(obj, (value, key) => {
            if (!_.isEqual(typeof value, 'object') && _.includes(allowedTypes, typeof value)) {
                if (!_.isUndefined(data[_.camelCase(key)]))
                    obj[_.camelCase(key)] = data[_.camelCase(key)];
            } else {
                if (!_.isUndefined(data[_.camelCase(key)]))
                    this.mapObject(value, data[_.camelCase(key)]);
            }
        });
    }
}