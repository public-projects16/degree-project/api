import { CONSTANTS } from "../utils/Const";

export class User {

    private id: number;
    private name: string;
    private last_name: string;
    private email: string;
    private birthday: string;
    private type: string;
    private status: number;

    constructor() {
        this.id = CONSTANTS.INITIAL_ID;
        this.name = "";
        this.last_name = "";
        this.email = "";
        this.birthday = "";
        this.type = "";
        this.status = CONSTANTS.INITIAL_ID;
    }

    get Id() { return this.id; }

    set Id(value: number) { this.id = value; }

    get Name() { return this.name; }

    set Name(value: string) { this.name = value; }

    get LastName() { return this.last_name; }

    set LastName(value: string) { this.last_name = value; }

    get Email() { return this.email; }

    set Email(value: string) { this.email = value; }
    
    get Birthday() { return this.birthday; }

    set Birthday(value: string) { this.birthday = value; }
    
    get Type() { return this.type; }

    set Type(value: string) { this.type = value; }

    get Status() { return this.status; }

    set Status(value: number) { this.status = value; }
}