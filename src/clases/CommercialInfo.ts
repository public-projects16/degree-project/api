import * as _ from  "lodash";

import { CONSTANTS } from "../utils/Const";
import { ICommercialInfo } from "../interfaces/IBDTables";

export class CommercialInfo {
    private id: number = CONSTANTS.INITIAL_ID;
    private name: string = "";
    private webUrl: string = "";
    private contactMail: string = "";
    private contactPhone: string = "";
    private socialNetwork: string = "";
    private commercialBusiness: string = "";
    private logo: string = "";

    constructor(information?: ICommercialInfo) {
        if (!_.isUndefined(information)) this.setInformationValues(information);
    }

    get Id(): number { return this.id; }

    get Name(): string { return this.name; }

    get WebUrl(): string { return this.webUrl; }

    get ContactMail(): string { return this.contactMail; }

    get ContactPhone(): string { return this.contactPhone ; }

    get SocialNetwork(): string { return this.socialNetwork; }

    get CommercialBusiness() { return this.commercialBusiness; }

    get Logo() { return this.logo; }

    set Id(id: number) { this.id = id; }

    set Name(name: string) { this.name = name; }

    set WebUrl(url: string) { this.webUrl = url; }

    set ContactMail(mail: string) { this.contactMail = mail; }

    set ContactPhone(phone: string) { this.contactPhone = phone; }

    set SocialNetwork(socialNetwork: string) { this.socialNetwork = socialNetwork; }

    set CommercialBusiness(value: string) { this.commercialBusiness = value; }

    set Logo(value: string) { this.logo = value; }

    /**
     * @private
     * @param {ICommercialInfo} information
     * @memberof CommercialInfo
     */
    private setInformationValues(information: ICommercialInfo): void {
        this.id = (information.id) ? information.id : CONSTANTS.INITIAL_ID;
        this.name  = information.name;
        this.webUrl  = information.webUrl;
        this.contactMail  = information.contactMail;
        this.contactPhone  = information.contactPhone;
        this.socialNetwork  = information.socialNetwork;
        this.commercialBusiness  = information.commercialBusiness;
        this.logo = information.logo ?  information.logo : this.logo;
    }
}