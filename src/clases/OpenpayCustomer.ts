import { IOpenPayCustomer } from "../interfaces/IOpenPay";
import { OpenPayAddress } from "./OpenpayAddress";

export class OpenPayCostumer {

    private _name: string = '';
    private _email: string = '';
    private _lastName: string = '';
    private _phoneNumber: string = '';
    private _address: OpenPayAddress;

    constructor() {
        this._address = new OpenPayAddress();
    }

    get name() { return this._name; }

    set name(value: string) { this._name = value; }

    get email() { return this._email; }

    set email(value: string) { this._email = value; }

    get lastName() { return this._lastName; }

    set lastName(value: string) { this._lastName = value; }

    get phoneNumber() { return this._phoneNumber; }

    set phoneNumber(value: string) { this._phoneNumber = value; }

    get adress() { return this._address; }

    set adress(value: OpenPayAddress) { this._address = value; }

    public toJSON(): IOpenPayCustomer {
        return {
            name: this.name,
            email: this.email,
            last_name: this.lastName,
            phone_number: this.phoneNumber,
            address: this.adress.toJSON()
        }
    }

}