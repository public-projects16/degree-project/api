export class OpenPayCard {

    private _cardNumber: string;
    private _holderName: string;
    private _expirationYear: string;
    private _expirationMonth: string;
    private _cvv2: string;
    private _brand: string;

    constructor() {
        this._cardNumber = "";
        this._holderName = "";
        this._expirationMonth = "";
        this._expirationYear = "";
        this._cvv2 = "";
        this._brand = "";
    }

    get cardNumber(): string { return this._cardNumber; }

    set cardNumber(value: string) { this._cardNumber = value; }

    get holderName(): string { return this._holderName; }

    set holderName(value: string) { this._holderName = value; }

    get expirationYear(): string { return this._expirationYear; }

    set expirationYear(value: string) { this._expirationYear = value; }

    get expirationMonth(): string { return this._expirationMonth; }

    set expirationMonth(value: string) { this._expirationMonth = value; }

    get cvv2(): string { return this._cvv2; }

    set cvv2(value: string) { this._cvv2 = value; }

    get brand(): string { return this._brand; }

    set brand(value: string) { this._brand = value; }

    public toJSON(): object {
        return {
            card_number: this.cardNumber,
            holder_name: this.holderName,
            expiration_year: this.expirationYear,
            expiration_month: this.expirationMonth,
            cvv2: this.cvv2,
            brand: this.brand
        }
    }
}