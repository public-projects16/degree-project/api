import { NextFunction, Request, Response } from "express";
import { ApiPath, ApiOperationGet} from "swagger-express-ts";

@ApiPath({
    name: "Example",
    path: "/api"
})


class ExampleController {

    constructor() {}
    
    @ApiOperationGet({
        description: "Example from controller with swagger decorator",
        summary: "Example from controller with swagger decorator",
        responses: {
            200: {
                description: "Success",
            }
        }
    })

    public index(req: Request, res: Response, next: NextFunction) {

       
    }
}

const example_controller: ExampleController = new ExampleController();
export default example_controller;