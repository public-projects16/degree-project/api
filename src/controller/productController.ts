import { Request, Response } from "express";
import multerConfig from "../configuration/multer";
import { IProduct } from "../interfaces/IBDTables";
import productsService from "../services/productsService";
import responses from "../utils/responses";

import * as _ from "lodash";
import { ExcelProductsNames } from "../utils/Const";
import { messages } from "../utils/messages";
import { Product } from "../clases/Product";
import { ApiOperationGet, ApiOperationPost, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";

interface MulterRequest extends Request {
    files: any;
}

@ApiPath({
    name: "Products",
    path: "/products"
})

class ProductController {

    @ApiOperationGet({
        path: "/show",
        description: "Muestra todos los productos relacionados a la empresa",
        responses: {
            200: {
                description: "success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public ShowProducts(req: Request, res: Response) {
        const token: any = req.headers.authorization;
        productsService.showProductsOnCompany(token)
        .then((products: IProduct[]) => {
            res.json(responses.getSuccessResponse('', products));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        });
    }

    @ApiOperationPost({
        path: "/masiveUpload/{id}",
        description: "Inserta todos los productos contenidos en el excel",
        parameters: {
            path: {
                id: {
                    description: 'Company id',
                    type: SwaggerDefinitionConstant.INTEGER
                }
            },
            body: {
                description: "Files path",
                type: SwaggerDefinitionConstant.Model.Property.Type.OBJECT,
                required: true
            }
        },
        responses: {
            200: {
                description: messages.products.masive_success_registering,
                type: SwaggerDefinitionConstant.JSON,
            },
            404: {
                description: messages.products.error_inserting_masive,
                type: SwaggerDefinitionConstant.JSON,
            }
        }
    })

    public masiveUpload(req: Request, res: Response) {
        multerConfig.ProductFileUpload(req, res, (error: Error) => {
            if (!error) {
                const files: any = (req as MulterRequest).files;
                const companyId = parseInt(req.params.id);
                productsService.insertMasiveProducts(_.head<any>(files[ExcelProductsNames.fieldname]), companyId )
                .then((numberOfProducts: number) => {
                    res.json(responses.getSuccessResponse( messages.products.masive_success_registering, [numberOfProducts] ));
                })
                .catch((productError: Error) => {
                    res.json(responses.getErrorResponse(productError));
                })
            } else {
                res.json(responses.getErrorResponse(error));
            }
        });
    }

    @ApiOperationPost({
        path: "/add/{id}",
        description: "Registra un nuevo producto",
        parameters: {
            path: {
                id: {
                    description: 'Company id',
                    type: SwaggerDefinitionConstant.INTEGER
                }
            },
            body: {
                description: "Product",
                type: SwaggerDefinitionConstant.Model.Property.Type.OBJECT,
                model: "Product"
            }
        },
        responses: {
            200: {
                description: messages.products.product_inserted_successfully,
                type: SwaggerDefinitionConstant.JSON,
            },
            404: {
                description: messages.products.error_inserting,
                type: SwaggerDefinitionConstant.JSON,
            }
        }
    })

    public add(req: Request, res: Response) {
        const newProduct: Product = new Product(req.body); 
        const companyId: number = parseInt(req.params.companyId);
        productsService.insertProduct(newProduct, companyId).then((message: string) => {
            res.json(responses.getSuccessResponse(message));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationPost({
        path: "/remove",
        description: "Elimina el producto seleccionado",
        parameters: {
            body: {
                description: "Product",
                type: SwaggerDefinitionConstant.Model.Property.Type.OBJECT,
                model: "Product"
            }
        },
        responses: {
            200: {
                description: messages.products.removed_successfully,
                type: SwaggerDefinitionConstant.JSON,
            },
            404: {
                description: messages.products.error_removing,
                type: SwaggerDefinitionConstant.JSON,
            }
        }
    })

    public remove(req: Request, res: Response) {
        const productToRemove: Product = new Product(req.body);
        productsService.removeProduct(productToRemove)
        .then((message: string) =>{ 
            res.json(responses.getSuccessResponse(message));
        })
        .catch((error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationPost({
        path: "/edit",
        description: "Edita el producto seleccionado",
        parameters: {
            body: {
                description: "Product",
                type: SwaggerDefinitionConstant.Model.Property.Type.OBJECT,
                model: "Product"
            }
        },
        responses: {
            200: {
                description: messages.products.edited_successfully,
                type: SwaggerDefinitionConstant.JSON,
            },
            404: {
                description: messages.products.error_editing,
                type: SwaggerDefinitionConstant.JSON,
            }
        }
    })

    public edit(req: Request, res: Response) {
        const productToEdit: Product = new Product(req.body);
        productsService.editProduct(productToEdit)
        .then((message: string) => {
            res.json(responses.getSuccessResponse(message));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationGet({
        path: "/productToSell/{id}",
        description: "Obtiene todos los productos relacionados a una empresa",
        parameters: {
            path: {
                companyId: {
                    description: "Company id",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        },
        responses: {
            200: {
                description: "success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public getProductsToSell(req: Request, res: Response) {
        let companyId = parseInt(req.params.companyId);
        productsService.getProductsByCompanyId(companyId)
        .then((products: Product[]) => {
            res.json(responses.getSuccessResponse('', products));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }
}

const productController: ProductController = new ProductController();
export default productController;