import { NextFunction, Request, Response } from "express";
import passport from "passport";
import { authenticationStrategy } from "../auth/B2CAuth";

export class AuthController {

    constructor() {}
    
    public authenticate(req: Request, res: Response, next: NextFunction) {
        passport.use(authenticationStrategy);
        passport.authenticate("oauth-bearer", {session: false});
    }
}
