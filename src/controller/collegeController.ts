import { Request, Response } from "express";
import { ApiOperationGet, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";
import { College } from "../clases/College";
import collegeService from "../services/collegeService";
import { messages } from "../utils/messages";
import responses from "../utils/responses";

@ApiPath({
    name: "College",
    path: "/college"
})
class CollegeController {

    @ApiOperationGet({
        path: "/show/names",
        description: "Muestra los nombres de todas las instituciones",
        responses: {
            200: {
                description: "Success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public ShowNames(req: Request, res: Response) {
        collegeService.getNames().then((institutions: Array<College>) => {
            res.json(responses.getSuccessResponse('', institutions));
        }).catch(() => {
            res.json(responses.getErrorResponse(messages.general.retreiving_data_error));
        });
    }

    @ApiOperationGet({
        path: "/campus/{institute}",
        description: "Muestra los campus de una institución",
        parameters: {
            path: {
                institution: {
                    description: "Nombre de la institución",
                    type: SwaggerDefinitionConstant.STRING,
                    required: true
                }
            }
        },
        responses: {
            200: {
                description: "Success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public GetCollegeByInstitution(req: Request, res: Response) {
        const institution: string = req.params.institute;
        collegeService.getCollegeByInstitution(institution).then((colleges: Array<College>) => {
            res.json(responses.getSuccessResponse('', colleges));
        }).catch(() => {
            res.json(responses.getErrorResponse(messages.general.retreiving_data_error));
        });
    }
}

const collegeController: CollegeController = new CollegeController();
export default collegeController;