import { Request, Response } from "express";
import { JWT } from "../utils/jwt";
import userService from "../services/userService";
import responses from "../utils/responses";
import { UserStatus } from "../utils/Const";
import { messages } from "../utils/messages";
import { ApiOperationGet, ApiOperationPut, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";

@ApiPath({
    name: "User",
    path: "/user"
})

class UserController {

    @ApiOperationGet({
        path: "/",
        description: "Get the user information requested",
        responses: {
            200: {
                description: "Success",
                type: SwaggerDefinitionConstant.JSON
            },
            404: {
                description: "Error getting the user",
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public async getUserInfo(req: Request, res: Response) {
        const token: string =  (req.headers.authorization) ? req.headers.authorization : '';
        const mail: string = JWT.getMailOwner(token);
        const response = await userService.getUserInfo(mail);
        res.json(responses.getSuccessResponse("Success", [response]));
    }

    @ApiOperationPut({
        path: "/disable",
        description: "Disable the requested user",
        parameters: {
            body: {
                description: "The user id",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "UserId"
            }
        },
        responses: {
            200: {
                description: "Success",
                type: SwaggerDefinitionConstant.JSON
            },
            404: {
                description: messages.account.disabled_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public disableUser(req: Request, res: Response) {
        const userId: number = parseInt(req.body.userId);
        userService.changeUserStatus(userId, UserStatus.disabled)
        .then(() => {
            res.json(responses.getSuccessResponse(messages.account.disabled_successfully));
        })
        .catch(() => {
            res.json(responses.getErrorResponse(messages.account.disabled_error));
        })
    }

    @ApiOperationPut({
        path: "/reactivate",
        description: "Disable the requested user",
        parameters: {
            body: {
                description: "The user id",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "UserId"
            }
        },
        responses: {
            200: {
                description: messages.account.reactivated_succesfully,
                type: SwaggerDefinitionConstant.JSON
            },
            404: {
                description: messages.account.reactivation_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public reactivateUser(req: Request, res: Response) {
        const userId: number = parseInt(req.body.userId);
        userService.changeUserStatus(userId, UserStatus.available)
        .then(() => {
            res.json(responses.getSuccessResponse(messages.account.reactivated_succesfully));
        })
        .catch(() => {
            res.json(responses.getErrorResponse(messages.account.reactivation_error));
        })
    }

}

const userController: UserController = new UserController();
export default userController;