import { Request, Response } from "express";
import { Company } from "../clases/company";
import { OpenPayClass } from "../clases/OpenPayClass";
import openpayService from "../services/OpenpayService";
import { JWT } from "../utils/jwt";

class ProofController {


    public index(req: Request, res: Response) {
        res.json({msg: "This is a proof using azure B2C"});
    }

    public loggedIn(req: Request, res: Response) {
        const token: any = req.headers.authorization;
        const decoded_token: any = JWT.decodeHeaderToken(token);
        res.json({status: "success", data: { info: decoded_token }  , err: {}});
    }

    public openPay(req: Request, res: Response) {
        let openpay: OpenPayClass = new OpenPayClass();
        openpay.load(req.body);
        openpayService.createTransaction(openpay, new Company())
        .then((result: any) => {
            res.json(result);
        })
        .catch((error) => {
            res.send(error);
        })
    }
}

const proofController: ProofController = new ProofController();
export default proofController;