import { Request, Response, NextFunction } from "express";
import {  IWorker } from "../interfaces/IBDTables";
import { roleNames, system_status } from "../utils/rolesNames";
import { JWT } from "../utils/jwt";
import responses from "../utils/responses";
import { messages } from "../utils/messages";
import workerService from "../services/workerService";
import { Company } from "../clases/company";
import { ApiOperationGet, ApiOperationPost, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";

@ApiPath({
    name: "Worker",
    path: "/worker"
})

class WorkerController {

    @ApiOperationPost({
        path: "/register",
        description: "Register a new worker and create a new worker request",
        parameters: {
            body: {
                description: "Worker information",
                type: SwaggerDefinitionConstant.JSON,
                model: "User"
            }
        },
        responses: {
            200: {
                description: "success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: 'Error creating the new worker',
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public async Register(req: Request, res: Response, next: NextFunction) {
        const token = (req.headers.authorization) ? req.headers.authorization : '';
        if (token.length > 0) {
            const new_worker: IWorker = {
                user: {
                    name:req.body.user.name,
                    last_name: req.body.user.lastname,
                    birthday: req.body.user.birthday,
                    email: JWT.getMailOwner(token),
                    type: roleNames.worker,
                    status: system_status.waiting
                },
                a_access_key: req.body.a_access_key,
                b_access_key: req.body.a_access_key
            };
            workerService.insertNewWork(new_worker)
            .then((worker: IWorker) => {
                res.json(responses.getSuccessResponse(messages.worker.added_successfully, [] , 200));
            })
            .catch((error: Error) => {
                res.json(responses.getErrorResponse(error, 400));
            });
        }
        else res.json(responses.getErrorResponse(messages.general.auth_error, 401));
    }

    @ApiOperationGet({
        path: "/company/{userId}",
        description: "Get the company where the user works",
        parameters: {
            path: {
                userId: {
                    required: true,
                    type: SwaggerDefinitionConstant.Parameter.Type.INTEGER,
                }
            }
        },
        responses: {
            200: {
                description: "success",
                type: SwaggerDefinitionConstant.JSON,
            },
            404: {
                description: "Error getting the requested company",
                type: SwaggerDefinitionConstant.JSON,
            }
        }
    })

    public GetCompanyInfo(req: Request, res: Response, next: NextFunction) {
        let userId: number = parseInt(req.params.userId);
        workerService.getCompanyWhereWorks(userId)
        .then((company: Company) => {
            res.json(responses.getSuccessResponse('', [company]))
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }
}

const workerController: WorkerController = new WorkerController();
export default workerController;