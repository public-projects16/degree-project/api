import {  IMerchant } from "../interfaces/IBDTables";
import { ApiOperationPost, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";
import { JWT } from "../utils/jwt";
import { messages } from "../utils/messages";
import { Request, Response, NextFunction } from "express";
import { roleNames, system_status } from "../utils/rolesNames";
import merchantService from "../services/merchantService";
import responses from "../utils/responses";

@ApiPath({
    name: "Merchant",
    path: "/merchant"
})

class Merchant {

    @ApiOperationPost({
        path: "/register",
        description: "Registra un nuevo comerciante.",
        parameters: {
            body: {
                description: "Merchant information",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "User"
            }
        },
        responses: {
            200: {
                description: "success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: 'Error creating the new merchant',
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public async Register(req: Request, res: Response, next: NextFunction) {
        const token = (req.headers.authorization) ? req.headers.authorization : '';
        const key = JWT.keyGenerator(JWT.getMailOwner(token), roleNames.merchant);
        if (token.length > 0) {
            const new_merchant: IMerchant = {
                user: {
                    name:req.body.user.name,
                    last_name: req.body.user.lastname,
                    birthday: req.body.user.birthday,
                    email: JWT.getMailOwner(token),
                    type: roleNames.merchant,
                    status: system_status.enabled
                },
                CURP: req.body.curp,
                a_access_key: key
            };
            const response = await merchantService.insertNewMerchant(new_merchant);
            if (response[0])
                res.json(responses.getSuccessResponse(messages.merchant.added_successfully))
            else
                res.json(responses.getErrorResponse(response[1], 400));
        }
        else res.json(responses.getErrorResponse(messages.general.auth_error, 401));
    }
}

const merchantController: Merchant = new Merchant();
export default merchantController;