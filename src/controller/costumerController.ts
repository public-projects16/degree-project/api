import { Request, Response, NextFunction } from "express";
import { ICostumer } from "../interfaces/IBDTables";
import { roleNames, system_status } from "../utils/rolesNames";
import { JWT } from "../utils/jwt";
import responses from "../utils/responses";
import { messages } from "../utils/messages";
import costumerService from "../services/costumerService";
import userService from "../services/userService";
import { ApiOperationGet, ApiOperationPost, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";

@ApiPath({
    name: "Costumer",
    path: "/costumer"
})
class CostumerController {

    @ApiOperationPost({
        path: "/register",
        parameters: {
            body: {
                description: "The user information",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "User"
            }
        },
        responses: {
            200: {
                description: messages.costumer.added_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: 'Ocurrio un error al registrar al cliente.',
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public async Register(req: Request, res: Response, next: NextFunction) {
        const token = (req.headers.authorization) ? req.headers.authorization : '';
        if (token.length > 0) {
            const new_costumer: ICostumer = {
                user: {
                    name:req.body.user.name,
                    last_name: req.body.user.lastname,
                    birthday: req.body.user.birthday,
                    email: JWT.getMailOwner(token),
                    type: roleNames.costumer,
                    status: system_status.enabled
                },
                NIP: req.body.nip
            };
            const CostumerResponse = await costumerService.insertNewCostumer(new_costumer);
            if (CostumerResponse[0])
                res.json(responses.getSuccessResponse(messages.costumer.added_successfully))
            else
                res.json(responses.getErrorResponse(CostumerResponse[1], 400));
        }
        else res.json(responses.getErrorResponse(messages.general.auth_error, 401));
    }

    @ApiOperationGet({
        path: "/validate",
        description: "Valida si el usuario está registrado en el sistema.",
        responses: {
            200: {
                description: messages.general.registered,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.not_registered,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public async Validate(req: Request, res: Response, next: NextFunction) {
        const token: string = (req.headers.authorization) ? req.headers.authorization : '';
        const owner: string = JWT.getMailOwner(token);
        const validation = await userService.isUserRegister(owner);
        if (validation[0])
            res.json(responses.getSuccessResponse(messages.general.registered, [validation[1]]));
        else 
            res.json(responses.getErrorResponse(messages.general.not_registered));
    }

    @ApiOperationGet({
        path: "/transactions/{id}",
        description: "Obtiene las transacciones realizadas por el usuario",
        parameters: {
            path: {
                id: {
                    description: "Id del cliente",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        },
        responses: {
            200: {
                description: messages.general.registered,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.not_registered,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public async Transactions(req: Request, res: Response, next: NextFunction) {
        const transactions: any = await costumerService.getTransactions( parseInt(req.params.id));
        res.json(responses.getSuccessResponse("ok", transactions));
    }
}

const costumerController: CostumerController = new CostumerController();
export default costumerController;