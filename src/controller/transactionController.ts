import { NextFunction, Request, Response } from "express";
import { ApiPath, ApiOperationGet, SwaggerDefinitionConstant, ApiOperationPost} from "swagger-express-ts";
import { OpenPayClass } from "../clases/OpenPayClass";
import { Transaction } from "../clases/Transaction";
import transactionService from "../services/transactionService";
import { messages } from "../utils/messages";
import responses from "../utils/responses";

@ApiPath({
    name: "Transactions",
    path: "/transactions"
})


class TransactionController {

    constructor() {}
    
    @ApiOperationPost({
        path: "/transact",
        description: "Crea una nueva transacción",
        parameters: {
            body: {
                description: "Transacción",
                type: SwaggerDefinitionConstant.Model.Property.Type.OBJECT,
                model: "Transaction"
            }
        },
        responses: {
            200: {
                description: messages.transactions.created_successfully,
                type: SwaggerDefinitionConstant.JSON,
            },
            404: {
                description: messages.general.persisting_data_error,
                type: SwaggerDefinitionConstant.JSON,
            }
        }
    })

    public makeTransaction(req: Request, res: Response, next: NextFunction) {
       let transaction: Transaction = new Transaction();
       let openpay: OpenPayClass = new OpenPayClass();
       openpay.load(req.body.openpay);       
       transaction.company.Id = parseInt(req.body.companyId);
       transaction.costumer.Id = parseInt(req.body.costumerId);
       transaction.concept = req.body.concept;
       transaction.shoppingTotal = req.body.shoppingTotal;
       transaction.products = req.body.products;
       transactionService.createTransaction(transaction, openpay)
       .then((createdTransaction: Transaction) => {
            res.json(responses.getSuccessResponse(messages.transactions.created_successfully, [createdTransaction]));
       })
       .catch((error: Error) => {
           res.json(responses.getErrorResponse(error));
       }) 
    }

    @ApiOperationGet({
        path: "/company/transact/{companyId}",
        description: "Muestra todas las transacciones relacionadas a la empresa",
        responses: {
            200: {
                description: "success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })

    public findCompanyTransactions(req: Request, res: Response, next: NextFunction) {
        const companyId = parseInt(req.params.companyId);
        transactionService.findCompanyTransactions(companyId).then((transactions: Transaction[]) => {
            res.json(responses.getSuccessResponse('', transactions));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }
}

const transactionController: TransactionController = new TransactionController();
export default transactionController;