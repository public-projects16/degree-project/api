import { Request, Response } from "express";
import { IMerchant, ICompanyData, IWorker } from "../interfaces/IBDTables";
import merchantService from "../services/merchantService";
import companyService from "../services/companyServices";
import responses from "../utils/responses";
import { messages } from "../utils/messages";
import { Company } from "../clases/company";
import multerConfig from "../configuration/multer";
import { JWT } from "../utils/jwt";

import * as _ from "lodash";
import { College } from "../clases/College";
import { CommercialInfo } from "../clases/CommercialInfo";
import userService from "../services/userService";
import { UserStatus } from "../utils/Const";
import { ApiOperationGet, ApiOperationPost, ApiOperationPut, ApiPath, SwaggerDefinitionConstant } from "swagger-express-ts";

interface MulterRequest extends Request {
    files: any;
}

@ApiPath({
    name: "Company",
    path: "/company"
})

class CompanyController {


    @ApiOperationGet({
        path: "/info/{id}",
        description: "Obtiene la información de la empresa",
        parameters: {
            path: {
                id: {
                    description: "Id del comerciante",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        }, 
        responses: {
            200: {
                description:"success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.not_registered,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public async ShowData(req: Request, res: Response) {
        const merchant_info: IMerchant = await  merchantService.getMerchantInfo(parseInt(req.params.id));
        if (merchant_info.id) {
            const company_info: Company | undefined = await companyService.getComercialInformation(merchant_info.id);
            if (company_info)
                res.json(responses.getSuccessResponse("ok", [company_info]));
            else
                res.json(responses.getWarnResponse(messages.company.not_registered)); 
        } else {
            res.json(responses.getErrorResponse(messages.merchant.not_registered));
        }
    }

    @ApiOperationGet({
        path: "/infoByCollege/{id}",
        description: "Obtiene la información de la empresa a partir del institución al cual se encuentra asociado",
        parameters: {
            path: {
                id: {
                    description: "Id del institución",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        }, 
        responses: {
            200: {
                description:"Success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: "Error",
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public ShowDataByCollegeId(req: Request, res: Response) {
        const collegeId: number = parseInt(req.params.id);
        companyService.getCompanyDataByCompanyId(collegeId).then((companyData: Company[]) => {
            res.json(responses.getSuccessResponse('ok', companyData));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        });
    }

    @ApiOperationPost({
        path: "/register",
        description: "Registra una nueva empresa",
        parameters: {
            body: {
                description: "Informacion de la empresa",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "commercialInfo"
            }
        },
        responses: {
            200: {
                description:"Success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.error_registering,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public async RegisterCompany(req: Request, res: Response) {
        const token:  string =  (req.headers.authorization) ? req.headers.authorization : '';
        const company: Company = new Company(req.body.company);
        const merchantInfo: IMerchant = await merchantService.getMerchantInfo(req.body.userId);
        const mail = JWT.getMailOwner(token);
        companyService.registerCompany(company, merchantInfo, mail).then((ids: any) => {
            res.json(responses.getSuccessResponse('', [ids]));
        }).catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        });
    }

    @ApiOperationPost({
        path: "/registerDocuments",
        description: "Registra los documentos legales de una empresa",
        parameters: {
            path: {
                id: {
                    description: "Id de la empresa",
                    type: SwaggerDefinitionConstant.OBJECT,
                    required: true
                }
            }
        },
        responses: {
            200: {
                description:messages.company.registered_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.documents_format_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public RegisterDocuments(req: Request, res: Response) {
        const companyId: number = parseInt(req.params.companyId);
        multerConfig.CompanyDocumentsUpload(req, res, (err: Error) => {
            if (!err) {
                const files: any = (req as MulterRequest).files;
                companyService.registerDocuments(files, companyId)
                .then(() => {
                    res.json(responses.getSuccessResponse(messages.company.registered_successfully));
                })
                .catch((error: Error) => {
                    res.json(responses.getErrorResponse(error));
                });
            }
            else {
                res.json(responses.getErrorResponse(messages.company.documents_format_error));
            }
        });
    }

    @ApiOperationPost({
        path: "/joinSchool/{companyId}",
        description: "Asocia una empresa con un institución",
        parameters: {
            path: {
                companyId: {
                    description: "Id de la empresa",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            },
            body: {
                description: "Id del institución",
                type: SwaggerDefinitionConstant.INTEGER,
                required: true
            }
        },
        responses: {
            200: {
                description: messages.company.registered_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.error_registering,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public JoinCompanyCollege(req: Request, res: Response) {
        const companyId: number = parseInt(req.params.companyId);
        const college: College = new College();
        college.Id = req.body.id;
        companyService.joinCompanyCollege(college, companyId)
        .then((message: string) => {
            res.json(responses.getSuccessResponse(message));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        });
    }

    @ApiOperationGet({
        path: "/companyData",
        description: "Obtiene la información de la empresa a partir del token de autorización",
        responses: {
            200: {
                description: "Success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public GetCompanyData(req: Request, res: Response) {
        const token: string | undefined = req.headers.authorization;
        if (!_.isUndefined(token)) {
           const email: string = JWT.getMailOwner(token);
           companyService.getCompanyDataByUserId(email).then((companyData: ICompanyData) => {
            if (!_.isUndefined(companyData))
                res.json(responses.getSuccessResponse('', [companyData]));
           })
           .catch((error: Error) => {
               res.json(responses.getErrorResponse(error));
           });
        } else {
            res.json(responses.getErrorResponse(messages.general.oauth_error))
        }
    }

    @ApiOperationPut({
        path: "/update/commercialInfo",
        description: "Actualiza la información comercial de una empresa",
        parameters: {
            body: {
                description: "Nueva información comercial",
                type: SwaggerDefinitionConstant.OBJECT,
                required: true,
                model: "commercialInfo"
            }
        },
        responses: {
            200: {
                description: messages.company.commercial_info.updated_correctly,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.commercial_info.error_updating,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public UpdateCommercialInfo(req: Request, res: Response) {
        const commercialInfo: CommercialInfo = new CommercialInfo(req.body);
        companyService.updateCommercialInfo(commercialInfo).then(() => {
            res.json(responses.getSuccessResponse(messages.company.commercial_info.updated_correctly))
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        });
    }

    @ApiOperationPost({
        path: "/registerLogo/{commercialInfoId}/{companyId}",
        description: "Registra el logo de la empresa",
        parameters: {
            path: {
                commercialInfoId: {
                    description: "Id de la informacion commercial",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                },
                companyId: {
                    description: "Id de la empresa",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        },
        responses: {
            200: {
                description: messages.company.commercial_info.logo_updated_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.commercial_info.logo_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public RegisterLogo(req: Request, res: Response) {
        multerConfig.CompanyLogo(req, res, async (error: Error) => {
            if(!error) {
                let files: any = (req as MulterRequest).files;
                let path: string = "";
                if (!_.isUndefined(files)) {
                    _.mapKeys(Object.keys(files), (key: any) => {
                        path =  _.head<any>(files[key]).path;
                    });
                    let commercialInfoId = parseInt(req.params.commercialInfoId);
                    let companyId = req.params.companyId;
                    companyService.registerLogo(path, commercialInfoId, companyId).then((message: string) => {
                        res.json(responses.getSuccessResponse(message));
                    })
                    .catch((err: Error) => {
                        res.json(responses.getErrorResponse(err));
                    })
                } else {
                    res.json(responses.getErrorResponse(messages.company.commercial_info.logo_error));
                }
            } else {
                res.json(responses.getErrorResponse(messages.company.commercial_info.logo_error));
            }
        })
    }

    @ApiOperationGet({
        path: "/showProspects/{userId}",
        description: "Obtiene todas las solicitudes de trabajadores en una empresa",
        parameters: {
            path: {
                userId: {
                    description: "Id de usuario del comerciante",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        },
        responses: {
            200: {
                description: "Succes",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public ShowProspects(req: Request, res: Response) {
        let userId: number = parseInt(req.params.userId);
        companyService.getProspects(userId)
        .then((candidates: IWorker[]) => {
            res.json(responses.getSuccessResponse('', candidates));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationPost({
        path: "/acceptWorker",
        description: "Se acepta la petición de un trabajador en una empresa",
        parameters: {
            body: {
                description: "Id del trabajador",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "UserId"
            }
        },
        responses: {
            200: {
                description: messages.worker.accepted_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public AcceptWorker(req: Request, res: Response) {
        let workerId = parseInt(req.body.workerId);
        companyService.acceptWorker(workerId)
        .then(() => {
            res.json(responses.getSuccessResponse(messages.worker.accepted_successfully));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationPost({
        path: "/declineWorker",
        description: "Se declina la petición de un trabajador en una empresa",
        parameters: {
            body: {
                description: "Id del trabajador",
                type: SwaggerDefinitionConstant.OBJECT,
                model: "UserId"
            }
        },
        responses: {
            200: {
                description: messages.worker.declined_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.worker.error_declining,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public DeclineWorker(req: Request, res: Response) {
        let userId = parseInt(req.body.userId);
        let rejected = UserStatus.rejected;
        userService.changeUserStatus(userId, rejected)
        .then(() => {
            res.json(responses.getSuccessResponse(messages.worker.declined_successfully));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationGet({
        path: "/workerOnCompany/{id}",
        description: "Obtiene la información de la empresa a partir de un trabajador",
        parameters: {
            path: {
                id: {
                    description: "Id del trabajador",
                    type: SwaggerDefinitionConstant.INTEGER,
                    required: true
                }
            }
        }, 
        responses: {
            200: {
                description:"Success",
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.general.retreiving_data_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public WorkerOnCompany(req: Request, res: Response) {
        let userId = parseInt(req.params.userId);
        companyService.getCompanyByWorker(userId)
        .then((company: Company) => {
            res.json(responses.getSuccessResponse('', [company]));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(error));
        })
    }

    @ApiOperationPut({
        path: "/disable",
        description: "Deshabilita una empresa",
        parameters: {
            body: {
                description: "Id de la empresa",
                type: SwaggerDefinitionConstant.OBJECT,
                required: true,
                model: "UserId"
            }
        },
        responses: {
            200: {
                description: messages.company.dusabled_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.disable_error,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public disableCompany(req: Request, res: Response) {
        const companyId: number = parseInt(req.body.companyId);
        companyService.changeCompanyStatus(companyId, UserStatus.disabled)
        .then(() => {
            res.json(responses.getSuccessResponse(messages.company.dusabled_successfully));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(messages.company.disable_error));
        })
    }

    @ApiOperationPut({
        path: "/reactivate",
        description: "Reactiva una empresa",
        parameters: {
            body: {
                description: "Id de la empresa",
                type: SwaggerDefinitionConstant.OBJECT,
                required: true,
                model: "UserId"
            }
        },
        responses: {
            200: {
                description: messages.company.reactivated_successfully,
                type: SwaggerDefinitionConstant.JSON
            },
            400: {
                description: messages.company.error_reactivating,
                type: SwaggerDefinitionConstant.JSON
            }
        }
    })
    public reactivateCompany(req: Request, res: Response) {
        const companyId: number = parseInt(req.body.companyId);
        companyService.changeCompanyStatus(companyId, UserStatus.available)
        .then(() => {
            res.json(responses.getSuccessResponse(messages.company.reactivated_successfully));
        })
        .catch((error: Error) => {
            res.json(responses.getErrorResponse(messages.company.error_reactivating));
        })
    }
}

const companyController: CompanyController = new CompanyController();
export default companyController;