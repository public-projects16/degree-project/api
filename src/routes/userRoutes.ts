import { Router } from "express";
import userController from "../controller/userController";

class UserRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof UserRoutes
     */
    private config(): void {
        this.router.get("/",  userController.getUserInfo);
        this.router.put("/disable", userController.disableUser);
        this.router.put("/reactivate", userController.reactivateUser);
    }
}

const userRoutes = new UserRoutes();
export default userRoutes.router;