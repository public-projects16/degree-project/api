import { Router } from "express";
import workerController from "../controller/workerController";

class WorkerRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof WorkerRoutes
     */
    private config(): void {
        this.router.post("/register", workerController.Register);
        this.router.get("/company/:userId", workerController.GetCompanyInfo)
    }
}

const workerRoutes = new WorkerRoutes();
export default workerRoutes.router;