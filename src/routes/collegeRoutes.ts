import { Router } from "express";
import collegeController from "../controller/collegeController";

class CompanyRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof CompanyRoutes
     */
    private config(): void {
        this.router.get("/show/names", collegeController.ShowNames);
        this.router.get("/campus/:institute", collegeController.GetCollegeByInstitution);
    }
}

const companyRoutes = new CompanyRoutes();
export default companyRoutes.router;