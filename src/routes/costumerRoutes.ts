import { Router } from "express";
import costumerController from "../controller/costumerController";

class CostumerRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof CostumerRoutes
     */
    private config(): void {
        this.router.post("/register", costumerController.Register);
        this.router.get('/validate', costumerController.Validate);
        this.router.get('/transactions/:id', costumerController.Transactions);
    }
}

const costumerRoutes = new CostumerRoutes();
export default costumerRoutes.router;