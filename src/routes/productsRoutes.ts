import { Router } from "express";
import productController from "../controller/productController";

class ProofRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    
    /**
     * @private
     * @memberof ProofRoutes
     */
    private config(): void {
        this.router.get("/show", productController.ShowProducts)
        this.router.post("/masiveUpload/:id",  productController.masiveUpload);
        this.router.post("/add/:companyId", productController.add);
        this.router.post("/remove", productController.remove);
        this.router.post("/edit", productController.edit);
        this.router.get("/productToSell/:companyId", productController.getProductsToSell);
    }
}

const proofRoutes = new ProofRoutes();
export default proofRoutes.router;