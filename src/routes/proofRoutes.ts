import { Router } from "express";
import proofController from "../controller/proofController";
import passport from "passport";

class ProofRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof ProofRoutes
     */
    private config(): void {
        this.router.get("/",  proofController.index);
        this.router.post("/openpay", proofController.openPay);
        this.router.get("/access", passport.authenticate("oauth-bearer", {session: false}), proofController.loggedIn);
    }
}

const proofRoutes = new ProofRoutes();
export default proofRoutes.router;