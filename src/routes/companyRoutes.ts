import { Router } from "express";
import companyController from "../controller/companyController";

class CompanyRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof CompanyRoutes
     */
    private config(): void {
        this.router.get("/info/:id", companyController.ShowData);
        this.router.get("/infoByCollege/:id", companyController.ShowDataByCollegeId);
        this.router.post("/register", companyController.RegisterCompany);
        this.router.post("/registerDocuments/:companyId", companyController.RegisterDocuments);
        this.router.post("/registerLogo/:commercialInfoId/:companyId", companyController.RegisterLogo);
        this.router.post("/joinSchool/:companyId", companyController.JoinCompanyCollege);
        this.router.get("/companyData", companyController.GetCompanyData);
        this.router.put("/update/commercialInfo", companyController.UpdateCommercialInfo);
        this.router.get("/showProspects/:userId", companyController.ShowProspects);
        this.router.post("/acceptWorker", companyController.AcceptWorker);
        this.router.post("/declineWorker", companyController.DeclineWorker);
        this.router.get("/workerOnCompany/:userId", companyController.WorkerOnCompany);
        this.router.put("/disable", companyController.disableCompany);
        this.router.put("/reactivate", companyController.reactivateCompany);
    }
}

const companyRoutes = new CompanyRoutes();
export default companyRoutes.router;