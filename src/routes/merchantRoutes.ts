import { Router } from "express";
import merchantController from "../controller/merchantController";

class MerchantRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof MerchantRoutes
     */
    private config(): void {
        this.router.post("/register",  merchantController.Register);
    }
}

const merchantRoutes : MerchantRoutes= new MerchantRoutes();
export default merchantRoutes.router;