import { Router } from "express";
import transactionController from "../controller/transactionController";

class TransactionRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof ExampleRoutes
     */
    private config(): void {
        this.router.post("/transact",  transactionController.makeTransaction);
        this.router.get("/company/transact/:companyId", transactionController.findCompanyTransactions);
    }
}

const transactionRoutes = new TransactionRoutes();
export default transactionRoutes.router;