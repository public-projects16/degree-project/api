import { Router } from "express";
import example_controller from "../controller/exampleController";

class ExampleRoutes {

    public router: Router = Router();

    constructor() {
        this.config();
    }
    
    /**
     * @private
     * @memberof ExampleRoutes
     */
    private config(): void {
        this.router.get("/",  example_controller.index);
    }
}

const example_routes = new ExampleRoutes();
export default example_routes.router;